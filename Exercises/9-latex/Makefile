all: gammareport.pdf

gammareport.pdf: gammafunc.tex gammafig.tex gamma_complexfig.tex
	pdflatex $<
	biber gammafunc
	pdflatex $<
	pdflatex $<

main.exe: main.cs math.dll
	mcs -reference:$(word 2, $^) $<

gammafig.tex: gamma.gpi
	gnuplot $<

gamma.gpi: plotdat.txt Makefile
	echo 'set term cairolatex input size 8.4cm,8.4cm' > $@
	echo "set output 'gammafig.tex'" >> $@
	echo "set key center right" >> $@
	echo "set tics out" >> $@
	echo "set xzeroaxis" >> $@
	echo "set yzeroaxis" >> $@
	echo "set xrange [-5:5]" >> $@
	echo "set yrange [-5:5]" >> $@
	echo "plot '$(word 1, $^)' i 1 u 1:2 w l notitle" >> $@

gamma_complexfig.tex: gamma_complex.gpi
	gnuplot $<

gamma_complex.gpi: plotdat.txt gammatab.txt Makefile
	echo 'set term cairolatex input size 8.4cm,8.4cm' > $@
	echo "set output 'gamma_complexfig.tex'" >> $@	
	echo 'set view 60, 30, 1, 1' >> $@
	echo "set pm3d" >> $@
	echo "set xrange [-5:5]" >> $@
	echo "set yrange [-5:5]" >> $@
	echo "set zrange [0:6]" >> $@
	echo "set xlabel 'Re(z)'" >> $@
	echo "set ylabel 'Im(z)'" >> $@
	echo "set zlabel '|{/Symbol G}(z)|' rotate left" >> $@
	echo "set title 'Absolute value of gamma-function for complex numbers'" >> $@
	echo 'set linetype 1 linecolor rgb "black"' >> $@
	echo 'splot "$(word 1, $^)" i 2 u 1:2:3 w pm3d notitle' >> $@

clean:
	$(RM) *.svg *.pdf *fig.tex *.log *.aux *.bcf *.gpi *.eps *.xml *blx*

