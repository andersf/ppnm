using System;
using static System.Console;
using static System.Math;
using static cmath;
using static complex;

class main{
	static int Main()
	{
		double eps = 1e-9; complex I = new complex(0,1);
		approxprint("Sqrt(2)", Sqrt(2), 1.4142135623731, eps);
		approxprint("e.pow(I)", exp(I), 0.540302306+0.841470985*I, eps);
		approxprint("e.pow(I*PI)",exp(I*PI),-1+0*I,eps);
		approxprint("i.pow(I)",I.pow(I),0.20787957635+0*I,eps);
		approxprint("sin(I*PI)",sin(I*PI),0+11.548739357257748*I,eps);
		approxprint("sinh(I)",sinh(I),0+0.841470985*I,eps);
		approxprint("cosh(I)",cosh(I),0.54030230586+0*I,eps);
		approxprint("sqrt(-1)",sqrt(-1+0*I),0+1*I,eps);
		approxprint("sqrt(I)",sqrt(0+1*I),0.70710678117655+0.70710678117655*I,eps);
	return 0;
	}
		
	public static void approxprint(string regn, double calc, double trueval, double acc)
	{
		Write($"{"Calculated",10}: {regn,11} = {calc,-15}\n"); 
		Write($"{"True value",10}: {"",11}   {trueval,-15}\n"); 
		if(approx(calc,trueval, acc, acc)) Write($"\tCorrect within a tolerance of {acc}\n\n");
		else Write($"\tIncorrect within a tolerance of {acc}\n\n");
	}
	
	public static void approxprint(string regn, complex calc, complex trueval, double acc)
	{
		Write($"{"Calculated",10}: {regn,11} = {calc,-15}\n"); 
		Write($"{"True value",10}: {"",11}   {trueval,-15}\n"); 
		if(approx(calc,trueval, acc, acc)) Write($"\tCorrect within a tolerance of {acc}\n\n");
		else Write($"\tIncorrect within a tolerance of {acc}\n\n");
	}
	
}
