using System;
using System.IO;
class fileio
{
	static int Main(string[] args)
	{ 
		if(args.Length < 2) return 1;
		string infile = args[0];
		string outfile = args[1];
		StreamReader instream = new StreamReader(infile);
		StreamWriter outstream = new StreamWriter(outfile,append:false);
		string x_string = "x"; 
		string cos = "cos(x)";
		outstream.WriteLine("{0}   {1}",x_string,cos.PadLeft(11)); 
		do{
			string line=instream.ReadLine();
			if(line==null)break;
			string[] words=line.Split(' ',',','\t');
			foreach(var word in words){
				double x=double.Parse(word);
				outstream.WriteLine(String.Format("{0,4:f5} {1,8:f5}",x,Math.Sin(x)));
			}
		}while(true);
		outstream.Close();
		instream.Close();
		return 0;
	}
}
