using System;
class cmdline
{
	static int Main(string[] args)
	{
		Console.WriteLine("x       sin(x)   cos(x)");
		foreach(var s in args)
		{
			double x = double.Parse(s);
			Console.WriteLine(String.Format("{0,-4:f4} {1,8:f5} {1,8:f5}",x,Math.Sin(x),Math.Cos(x)));
		}
	return 0;	
	}
}


