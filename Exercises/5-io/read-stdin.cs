using System;
using System.IO;
class stdinout{
	static int Main(){
		TextReader stdin  = Console.In;
		TextWriter stdout = Console.Out;
		string x_string = "x";
		string sin      = "sin(x)";
		string cos      = "cos(x)";
		stdout.WriteLine(String.Format("{0,-4} {1,8} {2,8}",x_string,sin,cos));
		do{
			string s =stdin.ReadLine();
			if(s==null)break;
			string[] words=s.Split(' ',',','\t');
			foreach(var word in words){
				double x=double.Parse(word);
				stdout.WriteLine(String.Format("{0,-4:f3} {1,8:f5} {2,8:f5}",x,Math.Sin(x),Math.Cos(x)));
			}
		}while(true);
	return 0;
	}
}

