using static System.Math;
using static System.Console;
using System.Collections.Generic;
using System;
//using static System.Double.PositiveInfinity;

public class main
{
	public static void Main()
	{
		Func<double,vector,vector> Logi = (t,y) => new vector(y[0]*(1-y[0]));

		double logi_x0 = 0, logi_x1 = 3;
		vector logi_start = new vector(0.5);
		List<double> xs_logi = new List<double>();
		List<vector> ys_logi = new List<vector>();
		//vector logifunc = 
		ode.rk23(Logi,logi_x0,logi_start,logi_x1,xs_logi,ys_logi);
		
		Error.WriteLine("\n");
		for(int i = 0; i<xs_logi.Count; i++)
		{
			Error.WriteLine($"{xs_logi[i]}\t{ys_logi[i][0]}");
		}


			
	}//Main
	
}//main

