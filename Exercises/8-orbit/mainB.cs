using static System.Math;
using static System.Console;
using System.Collections.Generic;
using System;
//using static System.Double.PositiveInfinity;

public class main
{
	public static void Main()
	{
		Func<double,vector,vector> equa_i   = (t,y) => new vector(y[1],1-y[0]+0.0001*y[0]*y[0]);
		Func<double,vector,vector> equa_ii  = (t,y) => new vector(y[1],1-y[0]+0*y[0]*y[0]);
		Func<double,vector,vector> equa_iii = (t,y) => new vector(y[1],1-y[0]+0.01*y[0]*y[0]);
		
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		ode.rk23(equa_i, 0, new vector(1,0), 2*PI, xlist:xs, ylist:ys, acc:1e-9, eps:1e-9);
		for(int i = 0; i<xs.Count; i++)
		{WriteLine($"{xs[i]}\t{ys[i][0]}");}
		

		xs = new List<double>(); ys = new List<vector>();
		ode.rk23(equa_ii, 0, new vector(1,-0.5), 2*PI, xlist:xs, ylist:ys, acc:1e-6, eps:1e-6);
		WriteLine("\n");
		for(int i = 0; i<xs.Count; i++)
		{WriteLine($"{xs[i]}\t{ys[i][0]}");}


		xs = new List<double>(); ys = new List<vector>();
		ode.rk23(equa_iii,0, new vector(1,-0.5), 16*Math.PI,xs,ys,acc:1e-3, eps:1e-3);
		
		WriteLine("\n");
		for(int i = 0; i<xs.Count; i++)
		{WriteLine($"{xs[i]}\t{ys[i][0]}");}
			
	}//Main
	
}//main

