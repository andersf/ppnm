using static System.Console;
using static System.Math;

public class approx_func{
	public static bool approx(double a, double b, double tau=1e-9, double epsilon=1e-9){
		if(Abs(a-b) < tau){Write($"\n{a} and {b} are equal with absolute precision {tau}\n");return true;}
		else if(Abs(a-b)/(Abs(a)+Abs(b)) < epsilon/2 ){Write($"\n{a} and {b} are equal with relative precision {epsilon}\n");return true;}
		else{
		Write($"\n{a} and {b} are not equal.\n");
		return false;}

	}
	public static void max_min_int(){
		int i=1;
		while(i<i+1){i++;}
		Write("Integer:\nMax found:    {0,-12}\n",i);
		Write($"int.MaxValue: {int.MaxValue,-28}\n");
	
		int j=1;
		while(j>j-1){j--;}
		Write("Min found:    {0,-12}\n",j);
		Write($"int.MinValue: {int.MinValue,-28}\n");
	}
	public static void machine_epsilon(){
		double x=1;
		while(1+x!=1){x/=2;}
		x*=2; 
		Write("\nMachine Epsilon\nDoubles: {0,16}\n",x);
		Write($"2^-52 =  {System.Math.Pow(2,-52)}\n");

		float y=1F;
		while((float)(y+1F)!=1){y/=2;}
		y*=2;
		Write("Floats:  {0,-16}\n",y);
		Write($"2^-23 =  {(float)(System.Math.Pow(2,-23))}\n");

	}


	public static void Harmonic_sum(){
		int max=int.MaxValue/2;
		float float_sum_up = 0f;
		for(int i = 1; i <= max; i++){
			float_sum_up += 1f/i;
		}
		Write("\nHarmonic sum, floats\nGoing up:   {0}\n", float_sum_up);	
		
		float float_sum_down = 0f;
		for(int i = max; i != 0; i--)	float_sum_down += 1f/i;
		Write("Going down: {0}\n", float_sum_down);	
		Write("Floats can only represents 7 digits while our max value is 10 digits\n");
	}

	public static void Harmonic_sum_double(){
		int max=int.MaxValue/2;
		double double_sum_up = 0;
		for(int i = 1; i <= max; i++){
			double_sum_up += 1d/i;
		}
		Write("\nHarmonic sum, double\nGoing up:   {0}\n", double_sum_up);	
		double double_sum_down = 0;
		for(int i = max; i != 0; i--) double_sum_down += 1d/i;
		Write("Going down: {0}\n", double_sum_down);	
		Write("Double can represent more digits allowing for higher precision and thus less difference between the 'summing up' and 'summing down' methods.\n");
	}


}
