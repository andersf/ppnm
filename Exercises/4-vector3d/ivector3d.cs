public interface ivector3d<T>
{
	double dot_product(T obj);
	T cross_product(T obj);
}//interface
