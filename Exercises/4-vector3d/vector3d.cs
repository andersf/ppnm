using static System.Math;

public struct vector3d : ivector3d<vector3d>
{
	private double _x,_y,_z;
	public double x{get{return _x;}set{_x=value;}}
	public double y{get{return _y;}set{_y=value;}}
	public double z{get{return _z;}set{_z=value;}}

	public vector3d(double x, double y, double z){
		this._x = x;
		this._y = y;
		this._z = z;
	}

	// print overload
	public void print(string s=""){
		System.Console.Write("{0} = ({1} {2} {3}) \n", s, this.x, this.y, this.z);
	}

	// addition operator
	public static vector3d operator+(vector3d v, vector3d u){
		return new vector3d(v.x + u.x, v.y + u.y, v.z + u.z);
	}
	// subtraction operator
	public static vector3d operator-(vector3d v, vector3d u){
		return v + (-1)*u;
	}
	// multiplication with number
	public static vector3d operator*(vector3d v, double a){
		return new vector3d(v.x * a, v.y * a, v.z * a);
	}
	public static vector3d operator*(double a, vector3d v){
		return v*a;
	}
		// dot product
	public double dot_product(vector3d other){
		return this.x * other.x + this.y * other.y + this.z * other.z;
	}
	// cross product
	public vector3d cross_product(vector3d other){
		return new vector3d(this.x*other.y - this.y*other.x, this.y*other.z - this.z*other.y, this.z*other.x - this.x*other.z);
	}

	// magnitude
	public double magnitude(){
		return Sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
	}
}

