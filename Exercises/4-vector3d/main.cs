using static System.Console;
using static System.Math;

class main{
	static int Main(){
	vector3d v = new vector3d(1,2,3);
	vector3d u = new vector3d(1,2,4);

	vector3d w = v + u;
	v.print("v");
	u.print("u");
	w.print("w");

	vector3d x1 = v*3;
	vector3d x2 = 4*v;
	x1.print("v*3");
	x2.print("4*v");

	double b = v.dot_product(u);
	Write("v.u = {0} \n", b);

	vector3d k = v.cross_product(u);
	k.print("v x u");
 
	double t = v.magnitude();
	Write("mag(v) = {0:f4} \n",t);
		
	return 0;
	}
}
