using static System.Math;
using System;
using static cmath;

public class main
{
	public static void Main()
	{
		for(double x = -3; x <= 3; x += 1.0/64)
		{
			Console.Error.WriteLine($"{x}\t{functions.erf(x)}");
		}//for
		Console.Error.WriteLine("\n\n");
		for(double x = -5; x <= 5; x += 1.0/128)
		{
			Console.Error.WriteLine($"{x}\t{functions.gamma(x)}");
		}//for
		Console.Error.WriteLine("\n\n");
		for(double re = -5; re <= 5; re += 1.0/32)
		{
			for(double im = -5; im <= 5; im += 1.0/32)
			{
				complex z = new complex(re, im);
				Console.Error.WriteLine($"{re}\t{im}\t{abs(functions.gamma_complex(z))}");
			}
			Console.Error.Write("\n");
		}//for
		Console.Error.WriteLine("\n\n");
		for(double x = 5; x <= 15; x += 1.0)
		{
			Console.Error.WriteLine($"{x}\t{functions.lngamma(x)}");
		}//for
	}//Main
}//main

