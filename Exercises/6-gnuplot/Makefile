all : plotdat.txt gamma.svg erf.svg gamma_complex.svg lngamma.svg

plotdat.txt: main.exe
	mono $< 2> $@

main.exe: main.cs math.dll 
	mcs -reference:$(word 2,$^) -out:$@ $<

math.dll: functions.cs ../../matlib/complex.cs ../../matlib/cmath.cs
	mcs -target:library -out:$@ $^ 

erf.svg: erf.gpi
	gnuplot $<

erf.gpi: main.exe plotdat.txt ErfTab.txt Makefile
	echo 'set term svg fname "Helvetica,18" background rgb "white"' > $@
	echo "set output 'erf.svg'" >> $@
	echo "set xlabel 'x'" >> $@
	echo "set ylabel 'f(x)'" >> $@
	echo "set key bottom right" >> $@
	echo "set title 'Error function'" >> $@
	echo "plot '$(word 2, $^)' i 0 u 1:2 w l t 'erf(x)', '$(word 3, $^)'u 1:2 t 'Tabulated values'  " >> $@

gamma.svg: gamma.gpi
	gnuplot $<

gamma.gpi: main.exe plotdat.txt GammaTab.txt Makefile
	echo 'set term svg fname "Helvetica,18" background rgb "white"' > $@
	echo "set output 'gamma.svg'" >> $@
	echo "set xlabel 'x'" >> $@
	echo "set key bottom right" >> $@
	echo "set tics out" >> $@
	echo "set xzeroaxis" >> $@
	echo "set yzeroaxis" >> $@
	echo "set xrange [-5:5]" >> $@
	echo "set yrange [-5:5]" >> $@
	echo "set ylabel 'f(x)'" >> $@
	echo "set title 'Gamma function'" >> $@
	echo "plot '$(word 2, $^)' i 1 u 1:2 w l t '{/Symbol G}(x)', '$(word 3, $^)'u 1:2 t 'Tabulated values'  " >> $@

lngamma.svg: lngamma.gpi
	gnuplot $<

lngamma.gpi: main.exe plotdat.txt Makefile
	echo 'set term svg fname "Helvetica,18" background rgb "white"' > $@
	echo "set output 'lngamma.svg'" >> $@
	echo "set xlabel 'x'" >> $@
	echo "set key top left" >> $@
	echo "set tics out" >> $@
	echo "set ylabel 'ln({/Symbol G}(x))'" >> $@
	echo "set title 'lngamma function'" >> $@
	echo "plot '$(word 2, $^)' i 3 u 1:2 w l t 'ln({/Symbol G}(x))'" >> $@

gamma_complex.svg: gamma_complex.gpi
	gnuplot $<

gamma_complex.gpi: main.exe plotdat.txt GammaTab.txt Makefile
	echo 'set term svg fname "Helvetica,18" background rgb "white"' > $@
	echo "set output 'gamma_complex.svg'" >> $@	
	echo 'set view 60, 30, 1, 1' >> $@
	echo "set pm3d" >> $@
	echo "set xrange [-5:5]" >> $@
	echo "set yrange [-5:5]" >> $@
	echo "set zrange [0:6]" >> $@
	echo "set xlabel 'Re(z)'" >> $@
	echo "set ylabel 'Im(z)'" >> $@
	echo "set zlabel '|{/Symbol G}(z)|' rotate left" >> $@
	echo "set title 'Absolute value of gamma-function for complex numbers'" >> $@
	echo 'set linetype 1 linecolor rgb "black"' >> $@
	echo 'splot "$(word 2, $^)" i 2 u 1:2:3 w pm3d notitle' >> $@

GammaTab.txt: Makefile
	echo 	'-1.5\t2.363272'	> $@
	echo 	'-0.5\t-3.544908'	>> $@
	echo 	'0.5\t1.772454'	>> $@
	echo 	'1\t1'	>> $@
	echo 	'1.5\t0.886227'	>> $@
	echo 	'2\t1'	>> $@
	echo 	'4\t6'	>> $@

ErfTab.txt: Makefile
	echo 	'0\t0\t1' > $@
	echo 	'0.1\t0.112462916\t0.887537084' >> $@
	echo 	'0.2\t0.222702589\t0.777297411' >> $@
	echo 	'0.3\t0.328626759\t0.671373241' >> $@
	echo 	'0.4\t0.428392355\t0.571607645' >> $@
	echo 	'0.5\t0.520499878\t0.479500122' >> $@
	echo 	'0.6\t0.603856091\t0.396143909' >> $@
	echo 	'0.7\t0.677801194\t0.322198806' >> $@
	echo 	'0.8\t0.742100965\t0.257899035' >> $@
	echo 	'0.9\t0.796908212\t0.203091788' >> $@
	echo 	'1\t0.842700793\t0.157299207' >> $@
	echo 	'1.1\t0.88020507\t0.11979493' >> $@
	echo 	'1.2\t0.910313978\t0.089686022' >> $@
	echo 	'1.3\t0.934007945\t0.065992055' >> $@
	echo 	'1.4\t0.95228512\t0.04771488' >> $@
	echo 	'1.5\t0.966105146\t0.033894854' >> $@
	echo 	'1.6\t0.976348383\t0.023651617' >> $@
	echo 	'1.7\t0.983790459\t0.016209541' >> $@
	echo 	'1.8\t0.989090502\t0.010909498' >> $@
	echo 	'1.9\t0.992790429\t0.007209571' >> $@
	echo 	'2\t0.995322265\t0.004677735' >> $@
	echo 	'2.1\t0.997020533\t0.002979467' >> $@
	echo 	'2.2\t0.998137154\t0.001862846' >> $@
	echo 	'2.3\t0.998856823\t0.001143177' >> $@
	echo 	'2.4\t0.999311486\t0.000688514' >> $@
	echo 	'2.5\t0.999593048\t0.000406952' >> $@
	echo 	'3\t0.99997791\t0.00002209' >> $@
	echo 	'3.5\t0.999999257\t0.000000743' >> $@ 

clean: 
	$(RM) *.dll main.exe [Oo]ut* *.svg *.gpi plot* *.pdf
