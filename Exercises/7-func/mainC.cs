using static System.Math;
using static System.Console;
using System;
//using static System.Double.PositiveInfinity;

public class main
{
	const double inf = double.PositiveInfinity;
	public static void Main()
	{
		double alpha = 0;
		for(int i = 0; alpha < 3.0; i++)
		{
			alpha = 0.1 + i*0.001;
			Func<double,double> psipsi  = (x) => Exp(-alpha*x*x);
			Func<double,double> psiHpsi = (x) => (-alpha*alpha*x*x/2+alpha/2+x*x/2)*Exp(-alpha*x*x);
			double Int_psipsi  = quad.o8av(psipsi ,-inf,inf);
			double Int_psiHpsi = quad.o8av(psiHpsi,-inf,inf);
			Console.WriteLine($"{alpha}\t{Int_psiHpsi/Int_psipsi}");
		}
	}//Main
}//main

