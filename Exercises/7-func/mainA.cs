using static System.Math;
using static System.Console;
using System;
//using static System.Double.PositiveInfinity;

public class main
{
	const double inf = double.PositiveInfinity;
	public static void Main()
	{
		Func<double,double> lnx_over_sqrtx = (x) => Log(x)/Sqrt(x);
		Func<double,double> exp_negx2	   = (x) => Exp(-x*x);
		Func<double,double> ln_1_over_x_2  = (x) => Pow(Log(1/x),2);
		Func<double,double> ln_1_over_x_3  = (x) => Pow(Log(1/x),3);
		Func<double,double> ln_1_over_x_4  = (x) => Pow(Log(1/x),4);

		Writer(lnx_over_sqrtx,"Log(x)/Sqrt(x)",0,1,-4,1e-6);
		Writer(exp_negx2,"Exp(-x*x)",-inf,inf,Sqrt(PI),1e-6);
		Writer(ln_1_over_x_2,"Pow(Log(1/x),2)",0,1,2,1e-6);
		Writer(ln_1_over_x_3,"Pow(Log(1/x),3)",0,1,6,1e-6);
		Writer(ln_1_over_x_4,"Pow(Log(1/x),4)",0,1,24,1e-6);
	}//Main
	
	public static void Writer(Func<double,double> f, string function, double a, double b, double truevalue, double acc)
	{
		double result = quad.o8av(f,a,b,acc,acc);
		double nacc = whichapprox(result,truevalue,acc,acc);
		WriteLine($"Integration of {function}:");
		WriteLine($"\tLower boundary: {a}");
		WriteLine($"\tUpper boundary: {b}");
		WriteLine($"\t{"Result",-10} = {result}\n\t{"True value",-10} = {truevalue}");
		if(approx(result,truevalue,nacc,nacc)) WriteLine($"\tCorrect within a {nacc} tolerance.\n");
		else WriteLine($"\tResult != True value, with a {nacc} tolerance.\n");
	}//Writer

	public static bool approx(double a, double b, double tau, double epsilon)
	{
		if(Abs(a-b) < tau) return true;
		else if(Abs(a-b)/(Abs(a)+Abs(b)) < epsilon/2 ) return true;
		else return false;
	}//approx
	
	public static double whichapprox(double a, double b, double tau, double epsilon)
	{
		while(tau <= 1 && epsilon <= 1)
		{
			if(Abs(a-b) < tau) break;
			else if(Abs(a-b)/(Abs(a)+Abs(b)) < epsilon/2 ) break;
			tau *= 10; epsilon *= 10;
		}
		return tau;
	}//whichapprox
}//main

