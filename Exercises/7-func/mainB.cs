using static System.Math;
using static System.Console;
using System;
//using static System.Double.PositiveInfinity;

public class main
{
	const double inf = double.PositiveInfinity;
	public static void Main()
	{
		Func<double,double> sqrtx_exp_negx = (x) => Sqrt(x)*Exp(-x);
		Func<double,double> x_o_ex_neg1    = (x) => x/(Exp(x)-1);
		Func<double,double> x3_o_ex_neg1   = (x) => x*x*x/(Exp(x)-1);
		Func<double,double> sinx_o_x       = (x) => Sin(x)/x;
		Writer(sqrtx_exp_negx,"Sqrt(x)*exp(-x)",0,inf,0.5*Sqrt(PI),1e-6);
		Writer(x_o_ex_neg1,"x/(exp(x)-1)",0,inf,PI*PI/6,1e-6);
		Writer(x3_o_ex_neg1,"x^3/(exp(x)-1)",0,inf,PI*PI*PI*PI/15,1e-3);
		Writer(sinx_o_x,"sin(x)/x",0,inf,PI/2,1e-1);
	}//Main
	
	public static void Writer(Func<double,double> f, string function, double a, double b, double truevalue, double acc)
	{
		double result = quad.o8av(f,a,b,acc,acc);
		double nacc = whichapprox(result,truevalue,acc,acc);
		WriteLine($"Integration of {function}:");
		WriteLine($"\tLower boundary: {a}");
		WriteLine($"\tUpper boundary: {b}");
		WriteLine($"\t{"Result",-10} = {result}\n\t{"True value",-10} = {truevalue}");
		if(approx(result,truevalue,nacc,nacc)) WriteLine($"\tCorrect within a {nacc} tolerance.\n");
		else WriteLine($"\tResult != True value, with a {nacc} tolerance.\n");
	}//Writer

	public static bool approx(double a, double b, double tau, double epsilon)
	{
		if(Abs(a-b) < tau) return true;
		else if(Abs(a-b)/(Abs(a)+Abs(b)) < epsilon/2 ) return true;
		else return false;
	}//approx
	
	public static double whichapprox(double a, double b, double tau, double epsilon)
	{
		while(tau <= 1 && epsilon <= 1)
		{
			if(Abs(a-b) < tau) break;
			else if(Abs(a-b)/(Abs(a)+Abs(b)) < epsilon/2 ) break;
			tau *= 10; epsilon *= 10;
		}
		return tau;
	}//approx

}//main

