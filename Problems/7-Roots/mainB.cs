using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
class main{

	static void Main()
	{
		List<double> XsforPlot = new List<double>(); 
		vector ResultsVectorforPlot = new vector(0);
		
		double rmax = 10;
		Func<vector,vector> M = (e) =>
		{
			double Tol = 1e-6;
			double smallR = Sqrt(Tol);
			vector y_start = new vector(smallR-smallR*smallR, 1-2*smallR);
			Func<double,vector,vector> Diffeq = (r,y) => new vector(y[1], -2*(1/r + e[0])*y[0]);
			rkstepXY F = new rkstepXY(Diffeq, smallR, rmax, y_start, acc0:Tol, eps0:Tol);
			XsforPlot = F.xValues;
			ResultsVectorforPlot = GetResultVector(F.yValues,0);
			return F.yValues.Last();
		};

		vector H_e0 = new vector(-0.44);
		vector H_atom = rootfinder.Newton(M,H_e0,eps:1e-3,dx:1e-9);
		
		H_e0.print("The starting guess for the root energy:",format:"{0,10:g7}");
		H_atom.print("The minimum energy found:",format:"{0,10:g7}");
		WriteLine("The true value: -0.5");
		WriteLine("See H_swave.svg for the function found in the calculation compared with f0(r)=r*exp(-r)");
		
		for(int i = 0; i < ResultsVectorforPlot.size; i++)
		{
			Console.Error.Write("{0}\t{1}\n",XsforPlot[i], ResultsVectorforPlot[i]);
		}		
	}//Main

	private static vector GetResultVector(List<vector> liste, int Index)
	{
		List<double> tmp = new List<double>();
		for(int i = 0; i< liste.Count; i++)
		{
			tmp.Add(liste[i][Index]);
		}//for
		return new vector(tmp.ToArray());
	}//GetResultVector
}
