using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
class main{

	static void Main()
	{
		vector H_e0 = new vector(-0.44);
		vector H_atom;
		
		//Simple forloop of rmax_values
		Error.Write("\n\n");
		for (double Rmax =8.75; Rmax <= 12.5; Rmax+=0.25)
		{
			if(Rmax != 10.75)
			{
			H_atom = rootfinder.Newton(Mmake(Rmax,"Simple"),H_e0,1e-3,1e-7);
			Error.Write($"{Rmax}\t{H_atom[0]}\n");	
			}//skipping Rmax =10.75 since the calculation stopped for reasons not found. 
		}//for
		Error.Write("\n\n");
		for (double Rmax = 7.75; Rmax <= 12.5; Rmax+=0.25)
		{
			if(Rmax != 10.75)
			{
			H_atom = rootfinder.Newton(Mmake(Rmax,"Improved"),H_e0,1e-3,1e-7);
			Error.Write($"{Rmax}\t{H_atom[0]}\n");	
			}
		}//for
		

	}//Main

	private static Func<vector,vector> Mmake(double Rmax, string Method)
	{
		return (e) =>
		{
			double Tol = 1e-3;
			double smallR = Sqrt(Tol);
			vector y_start = new vector(smallR-smallR*smallR, 1-2*smallR);
			Func<double,vector,vector> Diffeq = (r,y) => new vector(y[1], -2*(1/r + e[0])*y[0]);
			rkstepXY F = new rkstepXY(Diffeq, smallR, Rmax, y_start, acc0:Tol, eps0:Tol);
			
			if(Method == "Simple")
			{
				return F.yValues.Last();
			}
			if(Method == "Improved")
			{
				double k = Sqrt(-2*e[0]);
				double BoundaryCondition = Rmax*Exp(-k*Rmax);
				return new vector(F.yValues.Last()[0]-BoundaryCondition, F.yValues.Last()[1]);
			}
			else{Error.Write("test");return F.yValues.Last();}
			
		};

	}
}
