using System;
using static System.Console;
class main{

	static void Main()
	{
		Func<vector,vector> Rosenbrock = (x) => new vector(-2*(1-x[0])-400*x[0]*(x[1]-x[0]*x[0]),200*(x[1]-x[0]*x[0]));
		vector R_guess = new vector(10.0,10.0);
		vector R_root = rootfinder.Newton(Rosenbrock, R_guess, eps:1e-3, dx:1e-6);
		WriteLine($"The root of the RosenBrock function is calculated to be ({R_root[0]},{R_root[1]}).\nThis is calculated from the start guess ({R_guess[0]},{R_guess[1]})");
	       	WriteLine($"The real root is (1,1)");

	}//Main
}
