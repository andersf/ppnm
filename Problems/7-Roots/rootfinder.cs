using System;
using static System.Math;
using System.IO;
using System.Linq;
using System.Collections.Generic;

public class rootfinder
{
	//simple backtracking linesearch
	//Derivatives in Jacobian matrix are calc. numerically u finite diff.
	public static vector Newton(
		Func<vector,vector> f, 
		vector x, 
		double eps, 
		double dx)
	{
		double lambda = 1;
		while(f(x).norm() > eps)
			x = N_stepper(f,x,lambda,dx);
		return x; // root 
	}//Newton	

	public static vector N_stepper(
		Func<vector,vector> f,
		vector x,
		double lambda,
		double dx = 1e-7
		)
	{
		matrix J = Jacobian(f,x,dx);
		var QRJ = new GivensRotation(J);
		vector fx = f(x); //To make sure vector dimensions fit J-dimensions.
		vector x_step = QRJ.Givens_solve((-1)*fx);

		Func<double,double> g = (lambda_g) => 0.5*Pow(f(x+lambda_g*x_step).norm(),2);
		//double g0 	= 0.5*Pow(f(x).norm(),2);

		while(f(x+lambda*x_step).norm() > (1-lambda/2)*f(x).norm() && lambda > 1.0/64.0) // 1/64 is chosen as a generic small number. 
		{
			//More refined backtracker
			double c = (g(lambda)-(0.5-lambda)*Pow(f(x).norm(),2))/lambda/lambda;
			lambda   = Pow(f(x).norm(),2)/(2*c);
		}//while
		return x+lambda*x_step;
	}//Stepper

	public static matrix Jacobian(
		Func<vector,vector> f,
		vector x,
		double dx)
	{
		matrix J = new matrix(x.size,x.size);
		for(int i = 0; i < x.size; i++)
		{
			for(int k = 0; k < x.size; k++)
			{
				vector x_dx = x.copy(); x_dx[k] += dx;
				J[i,k] = (f(x_dx)[i]-f(x)[i])/dx;
			}//j-loop
		}//i-loop
		return J;
	}//Jacobian
}//rootfinder 
