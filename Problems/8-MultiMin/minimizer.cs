using System;
using static System.Math;
using System.IO;
using System.Linq;
using System.Collections.Generic;

public class minimizer
{
	//Quasi-Newton Minimization method
	//Numerical gradient roofinder.Jacobian
	//Back-tracking linesearch
	//rank-1 update	
	public int nsteps;
	public vector minima;

	
	public minimizer(
		Func<vector,double> f, 
		vector x, 
		double eps)
	{
			nsteps = 0;
			minima = QNewton(f,x,eps);
	}	

	public vector QNewton(
		Func<vector,double> f, 
		vector x, 
		double eps)
	{
		matrix B = new matrix(x.size,x.size); B.set_unity();
		while(Dphi(f,x).norm() > eps)
		{
			nsteps++;
			vector s = backtracker(f,B,x);
			vector y  = Dphi(f,x+s)-Dphi(f,x); //Dphi(f,x) is the gradient of f in x. 
			vector u  = s - B*y;
			x += s;
			if(Abs(u%y) > 1e-6){matrix dB = matrix.outer(u,u/(u%y)); B += dB;}
			else B.set_unity();
		}//while
		return x;
	}//SR1
	public static vector backtracker(Func<vector,double> f, matrix B, vector x)
	{
		double lambda = 1;
		
		vector Dx = (-1)*B*Dphi(f,x);
		vector s = lambda * Dx;
		vector Dphix = Dphi(f,x);
		double alpha = 1e-4;
		while(f(x+s) > f(x) + alpha*(s%Dphix) && lambda > 1.0/Pow(2,22)) //1/64 is chosen as a generic small number. 
		{
			lambda /= 2;
			s = lambda * Dx;
		}//while
		if(lambda < 1.0/Pow(2,22)) B.set_unity();
		//No reset of the Hessian as this proved to ruin the code. 
		return s;
	}//backtracker

	public static vector Dphi(
		Func<vector,double> f,
		vector x)
	{
		double dx = 1e-7; //tiny step. approx sqrt(machine epsilon);
		vector J = new vector(x.size);
		double fx = f(x);
		vector x_dx = x.copy();
		for(int i = 0; i < x.size; i++)
		{
			double xdx = Abs(x[i])*dx; 
			x_dx[i] += xdx;
			J[i] = (f(x_dx)-fx)/xdx;
			x_dx[i] -= xdx;
		}//i-loop
		return J;
	}//Jacobian

}//minimizer 
