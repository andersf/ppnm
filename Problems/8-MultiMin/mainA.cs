using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{

		Func<vector,double> Rosenbrock = (x) => {return Pow((1-x[0]),2)+100*Pow(x[1]-x[0]*x[0],2);};
		vector R_guess = new vector(10,10);
		vector R_start = R_guess.copy();
		minimizer R_min = new minimizer(Rosenbrock,R_guess,1e-4);
		DownhillSimplex.Writer("RosenBrock", R_min.minima, R_start, R_min.nsteps); 


		Func<vector,double> Himmelblau = (x) => {return Pow((x[0]*x[0]+x[1]-11),2)+Pow(x[0]+x[1]*x[1]-7,2);};
		vector HB_guess = new vector(-3,3);
		vector HB_start = HB_guess.copy();
		minimizer HB_min = new minimizer(Himmelblau,HB_guess,1e-4);
		DownhillSimplex.Writer("Himmelblau", HB_min.minima, HB_start, HB_min.nsteps); 
	
	
	}//Main


}//main
