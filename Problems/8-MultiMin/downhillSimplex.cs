using System;
using static System.Math;
using static System.Console;

public class DownhillSimplex
{
	//private vector pce, phi, pre, pex, pco, ps[lo];
	private vector[] ps;
	public int nsteps;
	public vector minima;

	public DownhillSimplex(
		Func<vector,double> f,
		vector startvector,
		double Tol=1e-3,
		double PercStep=0.1)
	{
		nsteps = 0;
		Makeps(startvector, PercStep);
		minima = SimplexUpdate(f,ps,Tol);
	}//constructor

	public void Makeps(vector startvector, double PercStep)
	{
		ps = new vector[startvector.size+1];
		ps[startvector.size]=startvector.copy();
		for(int i = 0;i<startvector.size; i++)
		{
			ps[i] = startvector.copy();
			ps[i][i] += startvector.norm()*PercStep;
		}
	}

	public vector SimplexUpdate(
		Func<vector,double> f, 
		vector[] ps, 
		double Tol)
	{
		int dim = ps.Length;
		int lo = 0; 
		int hi = 0;
		vector pce = new vector(dim-1);
		while(SizeSimplex(ps,dim)>Tol)
		{
		pce = new vector(ps.Length-1);
		for(int i = 0; i<ps.Length; i++)
		{
			pce += ps[i];
			if(f(ps[hi]) < f(ps[i]))
			{
				hi = i;
			}
			if(f(ps[lo]) > f(ps[i]))
			{
				lo = i;
			}
		}//for
		pce -= ps[hi];
		pce /= ps.Length-1;
		vector pre = 2*pce-ps[hi];
		vector pex = 3*pce-2*ps[hi];
		vector pco = 0.5*(pce + ps[hi]);
		if(f(pre) < f(ps[lo]))
		{
			pex = pce + 2*(pce-ps[hi]);
			if(f(pex) < f(pre))
			{
				ps[hi] = pex;
			}//if pex<pre
			else
				ps[hi] = pre;
		}//if pre<ps[lo]
		else
		{
			if(f(pre) < f(ps[hi]))
			{
				ps[hi] = pre;
			}//if pre<ps[hi]
			else
				if(f(pco) < f(ps[hi]))
				{
					ps[hi] = pco;
				}//if pco<ps[hi]
				else
				{
					for(int i = 0; i<ps.Length; i++)
						if(ps[i] != ps[lo])
						{
							ps[i] = 0.5*(ps[i]+ps[lo]);
						}//while
				}//else 
				for(int i = 0; i<ps.Length; i++) if(ps[i] == ps[hi]) ps[i] = ps[hi];
		}//else				
		nsteps++;
		}//While
		return pce;
	}//SimplexUpdate
	public static double Distance(vector p0, vector p, int dim)
	{
		double dist_sqr = 0;
		for(int i = 0; i < dim; i++)
		{
			dist_sqr += Pow(p[i]-p0[i],2);
		}//for
		return Sqrt(dist_sqr);
	}//Distance

	public static double SizeSimplex(vector[] ps, int length)
	{
		double size = 0;
		for(int i = 1; i < length; i++)
		{
			//size = Max(size,(ps[0]-ps[i]).norm());
			double dist = Distance(ps[0],ps[i],length-1);
			if(dist>size) size = dist;
		}//for
		return size;
	}//SizeSimplex
	
	public static void Writer(string f, vector min, vector start, int steps)
	{
		WriteLine($"Minimization of the {f} function:");
		start.print("Starting guess");
		min.print("Minimum found:");
		WriteLine($"Steps used: {steps}\n");
	}
}//class
