using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
using System.IO;
class main{

	static void Main()
	{
		StreamReader datalines = new StreamReader("Higgs.txt");
		List<double> Energy = new List<double>();
		List<double> Energy_unc = new List<double>();
		List<double> error = new List<double>();
		string ln;
		while((ln = datalines.ReadLine()) != null)
		{
		
			var Line = ln.Split('\t');
			Energy.Add(double.Parse(Line[0]));
			Energy_unc.Add(double.Parse(Line[1]));
			error.Add(double.Parse(Line[2]));
		}//while
		
		Func<double,vector,double> BreitWigner = (E,x) => {return x[2]/((E-x[0])*(E-x[0])+x[1]*x[1]/4);};
		Func<vector,double> BreitWignerdev = (x) =>
		{
			double sum = 0;
			for(int i = 0; i<Energy.Count; i++)
			{
				sum += Pow((BreitWigner(Energy[i],x)-Energy_unc[i]),2)/error[i]/error[i];
			}//for
			return sum;
		};//BreitWignerdev
			

		double tol = 1e-3;
		vector BW_guess = new vector(122,1,1);
		minimizer BW_min = new minimizer(BreitWignerdev,BW_guess,tol);
		BW_min.minima.print("Minimum for the deviation function:");
		WriteLine($"The calculation was done in {BW_min.nsteps} steps with a gradient tolerance of {tol}");
		WriteLine($"The mass (m) and width (G) of the Higgs boson is thus:\nm = {BW_min.minima[0]} GeV/c^2, G = {BW_min.minima[1]}");
		BW_guess.print("The starting guess was:");
		WriteLine($"To see the fit see Higgs.svg");

		for(double e = Energy[0]; e<Energy.Last(); e+=1.0/8)
			Error.WriteLine($"{e}\t{BreitWigner(e,BW_min.minima)}");

	}//Main
}//main
