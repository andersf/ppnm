using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
using System.IO;
class main{

	static void Main()
	{
				//........RosenBrock........

		Func<vector,double> Rosenbrock = (x) => {return Pow((1-x[0]),2)+100*Pow(x[1]-x[0]*x[0],2);};
		vector R_guess = new vector(2,2);
		vector R_start = R_guess.copy();
		DownhillSimplex R_min = new DownhillSimplex(Rosenbrock,R_guess,Tol:1e-6);
		DownhillSimplex.Writer("RosenBrock", R_min.minima, R_start, R_min.nsteps);

				//........Himmelblau.........
		
		Func<vector,double> Himmelblau = (x) => {return Pow((x[0]*x[0]+x[1]-11),2)+Pow(x[0]+x[1]*x[1]-7,2);};
		vector HB_guess = new vector(3,0);
		vector HB_Start = HB_guess.copy(); 
		DownhillSimplex HB_min = new DownhillSimplex(Himmelblau,HB_guess,Tol:1e-6);
		DownhillSimplex.Writer("HimmelBlau", HB_min.minima, HB_Start, HB_min.nsteps);
		
		
				//........Breit-Wigner.........
		
		StreamReader datalines = new StreamReader("Higgs.txt");
		List<double> Energy = new List<double>();
		List<double> Energy_unc = new List<double>();
		List<double> error = new List<double>();
		string ln;
		while((ln = datalines.ReadLine()) != null)
		{
		
			var Line = ln.Split('\t');
			Energy.Add(double.Parse(Line[0]));
			Energy_unc.Add(double.Parse(Line[1]));
			error.Add(double.Parse(Line[2]));
		}//while
		
		Func<double,vector,double> BreitWigner = (E,x) => {return x[2]/((E-x[0])*(E-x[0])+x[1]*x[1]/4);};
		Func<vector,double> BreitWignerdev = (x) =>
		{
			double sum = 0;
			for(int i = 0; i<Energy.Count; i++)
			{
				sum += Pow((BreitWigner(Energy[i],x)-Energy_unc[i]),2)/error[i]/error[i];
			}//for
			return sum;
		};//BreitWignerdev
		
		vector BW_guess_DS = new vector(127,1,2);
		vector BW_start_DS = BW_guess_DS.copy();
		DownhillSimplex BW_min_DS = new DownhillSimplex(BreitWignerdev,BW_guess_DS,Tol:1e-6);
		DownhillSimplex.Writer("Breit-Wigner", BW_min_DS.minima, BW_start_DS, BW_min_DS.nsteps);
	}//Main
}//main
