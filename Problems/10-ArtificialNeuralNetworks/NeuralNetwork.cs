using System;
using static System.Console;
using static System.Math;

public class ann
{
	private Func<double,double> f, deriv;
	private int n_hid; //number of hidden neutrons (one layer): 
	private vector param;
	public vector Param{get{return param;}}
	public int calls;
	public vector xk, yk;

	public ann(string Actifun, 
		   int N_hid, 
		   vector Xs, 
		   vector Ys) 
	{
	//Input (xk,yk) is the tabulated function values used to train the neural network
		WhichActivationFun(Actifun); this.n_hid = N_hid; this.xk = Xs; this.yk = Ys;
		calls = 0; 
		param = new vector(n_hid*3);//vector of parameters
		//WriteLine($"Calculation started using {Actifun} as activation function and {n_hid} hidden neurons");
		NeuralTraining();
		//WriteLine($"Calculation done using {Actifun} as activation function.");
	}//constructor	
	
	public void NeuralTraining()
	//training is optimizing the parameters by minimizing the difference between calculated y and tabulated y. 
	{
		vector xguess = new vector(n_hid*3);
		for(int i = 0; i < n_hid; i++)//Guess for the parameters.
		{
			xguess[i*3]  =xk[0]+(xk[xk.size-1]-xk[0])*i/(n_hid-1);
			xguess[i*3+1]=1;
			xguess[i*3+2]=1;
		}
		Func<vector,double> F = p =>
		{
			param = p;
			calls++;
			double dp = 0;
			for(int k = 0; k < xk.size; k++)
				dp += Pow(Feed(xk[k])-yk[k],2); //xk skal defineres
			return dp/xk.size;//for k
		};//F
		DownhillSimplex dp_min = new DownhillSimplex(F, xguess, Tol:1e-9, PercStep:0.1);  
		//minimizer dp_min = new minimizer(F, xguess, 1e-9);  
		param = dp_min.minima;
	}//NeuralTraining
	
	public double Deriv(double x)
	//Method to calculate the derivative by a simple weigted sum of the derivative of the function contributions from the single hidden neurons. 
	{
		double sum_deriv = 0;
		for(int i = 0; i < n_hid; i++)
		{
			sum_deriv += param[i*3+2]*1/param[i*3+1]*this.deriv((x-param[i*3])/param[i*3+1]);
		}
		return sum_deriv;
	}

	public double AntiDeriv(double b)
	//Method to calculate the antiderivative using the recursive integrator from previous problem.
	{
		Func<double,double> F = (x) => Feed(x);
		Integrators antideriv = new Integrators(F, xk[0], b, method:"Adaptive", abs:1e-6, eps:1e-6);
		return antideriv.Integral;
	}

	public double Feed(double x_input)
	//Method to calculate the final sum. 
	{
		double sum = 0;
		for(int i = 0; i < n_hid; i++)
			{sum += f((x_input-param[i*3])/param[i*3+1])*param[i*3+2];}
		return sum;
	}//SummationNeuron
	
	public void WhichActivationFun(string ActiFun)
	{
		if(ActiFun == "GaussWavelet")
		{
			this.f = (x) => x*Exp(-x*x);	
			this.deriv = (x) => Exp(-x*x)*(1-2*x*x);

		}
		else if(ActiFun == "Wavelet") 
		{
			this.f = (x) => Cos(5*x)*Exp(-x*x);
			this.deriv = (x) => -Exp(-x*x)*(5*Sin(5*x)+2*x*Cos(5*x));
		}
	}

}//integrator 
