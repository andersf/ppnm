using System;
using static System.Console;
using static System.Math;

public class annDiff
{
	
	//private Func<double,vector,vector> DiffEq;
	private Func<double,vector,double> ODE;
	private Func<double,double> f, deriv, doublederiv;
	private int n_hid; //number of hidden neutrons (one layer): 
	private vector param, BoundaryCond;
	private double BoundaryPoint, a, b;
	public vector Param{get{return param;}}
	public int calls;
	public vector IntegInterval;

	public annDiff(string Actifun, int N_hid, vector integinterval, Func<double,vector,double> ode, vector Boundarycond, double Boundarypoint)
	{
		WhichActivationFun(Actifun); this.n_hid = N_hid; this.IntegInterval = integinterval; this.a = integinterval[0]; this.b = integinterval[1]; this.ODE = ode; this.BoundaryCond = Boundarycond; this.BoundaryPoint = Boundarypoint; // this.DiffEq = Diffeq;
		calls = 0; 
		param = new vector(n_hid*3);//vector of params
		//WriteLine($"Calculation started using {Actifun} as activation function and {n_hid} hidden neurons. This takes about 30 seconds.");
		NeuralTraining();
		//WriteLine($"Calculation done using {Actifun} as activation function.");
	}//constructor	
	
	public void NeuralTraining()
	{
		vector xguess = new vector(n_hid*3);
		//double a = IntegInterval[0]; double b = IntegInterval[1];
		for(int i = 0; i < n_hid; i++)//Guess for the parameters.
		{
			xguess[i*3]  =a+(b-a)*i/(n_hid-1);
			xguess[i*3+1]=1;
			xguess[i*3+2]=1;
		}
		Func<vector,double> F = p =>
		{
			param = p;
			calls++;
			double dp = 0;
			Func<double,double> integrand = x => Pow(ODE(x, new vector(Feed(x), Deriv(x),DoubleDeriv(x))),2);
			Integrators number = new Integrators(integrand, a, b, method:"CC", abs:1e-3, eps:1e-3);
			dp += number.Integral;
			dp += (b-a)*Pow(Feed(BoundaryPoint)-BoundaryCond[0],2)+(b-a)*Pow(Deriv(BoundaryPoint)-BoundaryCond[1],2);
			//if(calls%10000==0)
			//Console.WriteLine($"calls = {calls} \t dp = {dp}");
			return dp;
		};//F
		DownhillSimplex dp_min = new DownhillSimplex(F, xguess, Tol:1e-4, PercStep:0.3);  
		param = dp_min.minima;
	}//NeuralTraining
	
	public double Deriv(double x)
	{
		double sum_deriv = 0;
		for(int i = 0; i < n_hid; i++)
			sum_deriv += param[i*3+2]*1/param[i*3+1]*this.deriv((x-param[i*3])/param[i*3+1]);
		return sum_deriv;
	}

	public double DoubleDeriv(double x)
	{
		double sum_doublederiv = 0;
		for(int i = 0; i < n_hid; i++)
			sum_doublederiv += param[i*3+2]*1/param[i*3+1]/param[i*3+1]*this.doublederiv((x-param[i*3])/param[i*3+1]);
		return sum_doublederiv;
	}

	public double Feed(double x_input)
	//Function to calculate the final sum. 
	{
		double sum = 0;
		for(int i = 0; i < n_hid; i++)
			sum += f((x_input-param[i*3])/param[i*3+1])*param[i*3+2];
		return sum;
	}//SummationNeuron
	
	public void WhichActivationFun(string ActiFun)
	{
		if(ActiFun == "GaussWavelet")
		{
			this.f = (x) => x*Exp(-x*x);	
			this.deriv = (x) => Exp(-x*x)*(1-2*x*x);
			this.doublederiv = (x) => Exp(-x*x)*x*(-6+4*x*x);

		}
		else if(ActiFun == "Gauss")
		{
			this.f = (x) => Exp(-x*x);	
			this.deriv = (x) => -2*x*Exp(-x*x);	
			this.doublederiv = (x) => (4*x*x-2)*Exp(-x*x);
		}
		else if(ActiFun == "Wavelet") 
		{
			this.f = (x) => Cos(5*x)*Exp(-x*x);
			this.deriv = (x) => -Exp(-x*x)*(5*Sin(5*x)+2*x*Cos(5*x));
			this.doublederiv = (x) => Exp(-x*x)*(20*x*Sin(5*x)-27*Cos(5*x)+4*x*x*Cos(5*x)); 
		}
		else if(ActiFun == "Cos") 
		{
			this.f = (x) => Cos(x);
			this.deriv = (x) => -Sin(x);
			this.doublederiv = (x) => -Cos(x);
		}
	}

}//integrator 
