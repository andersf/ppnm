using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		int NumNeu = 8;
		double a2 = 0, b2 = 5;
		vector integinterval_Bessel = new vector(a2,b2);
		Func<double,vector,double> ODE_Bessel = (x,y) => x*x*y[2]+2*x*y[1]+x*x*y[0]; 
		double BoundaryPoint2 = 1;
		vector Bessel_BoundaryCond = new vector(Sin(BoundaryPoint2),Cos(BoundaryPoint2)-Sin(BoundaryPoint2));
		
		for(double z = a2+0.001; z < b2; z += 0.1) 
			Error.WriteLine($"{z}\t{Sin(z)/z}");

		annDiff gausswavelet_Bessel = new annDiff("GaussWavelet",NumNeu,integinterval_Bessel, ODE_Bessel, Bessel_BoundaryCond, BoundaryPoint2);	
		Writer(gausswavelet_Bessel,a2,b2);
		WriteLine($"The solution to the 0th order spherical Bessel differential function using the boundarypoint c = {BoundaryPoint2} with y(c) = {Sin(BoundaryPoint2):f4} and y'(c) = {Cos(BoundaryPoint2)-Sin(BoundaryPoint2):f4}. The fit is found using the gaussian wavelet as activation function and with {NumNeu} hidden neurons and the downhill simplex minimization method.\nSee plot_differential.svg for the comparison with the true function.");
	}//Main

	public static void Writer(annDiff TypeAnnDiff, double a, double b, string Wish = "Func")
	{
	if(Wish == "Func")
	{
		Error.WriteLine("\n");
		for(double x = a; x <= b; x+=0.01)
		{
			Error.WriteLine($"{x}\t{TypeAnnDiff.Feed(x)}");
		}
	}
	}		
}//main
