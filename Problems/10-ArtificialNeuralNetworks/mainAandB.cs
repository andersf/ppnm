using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		Func<double,double> Wavelet_Andreas 	 = (x) => x*Cos(4*x)*Exp(-x*x);
		Func<double,double> Wavelet_Andreas_afledt 	 = (x) => Exp(-x*x)*((1-2*x*x)*Cos(4*x)-4*x*Sin(4*x));

		double a = -1, b = 1;
		int nx = 25;
		int NumNeu = 4;
		double[] xs = new double[nx], ys = new double[nx];
		
		for(int i = 0; i < nx; i++)
		{	
			xs[i] = a+(b-a)*i/(nx-1);
			ys[i] = Wavelet_Andreas(xs[i]);
			Error.WriteLine($"{xs[i]}\t{ys[i]}");
		}
		
		
		ann gausswavelet_ann = new ann("GaussWavelet",NumNeu,xs,ys);//"function");	
		ann wavelet_ann = new ann("Wavelet",NumNeu,xs,ys);//"function");	
		
		Writer(gausswavelet_ann,a,b);
		WriteLine($"The function fitted is: f(x) = x*Cos(4x)*Exp(-x*x).\nIt is fitted using {NumNeu} hidden neurons and The gaussian wavelet as activation function.");
		Writer(wavelet_ann,a,b);
		WriteLine($"It is also fitted using {NumNeu} hidden neurons and the wavelet function from the exercises as activation function.\nSee plot_Func.svg for the fit together with the true value points");
	
		Func<double,double> TrueIntegral = z => 
			{Integrators True = new Integrators(Wavelet_Andreas,a,z,method:"Adaptive",abs:1e-9,eps:1e-9);
			return True.Integral;};
		Error.WriteLine("\n");
		for(double z = a + 0.1; z <= b; z += 0.1) {Error.WriteLine($"{z}\t{TrueIntegral(z)}");}
		Writer(gausswavelet_ann,a,b,Wish:"AntiDeriv");
		WriteLine($"\nThe Antiderivative of the gaussian wavelet and the wavelet fit is found using the Adaptive recursive integrator method.\nSee plot_AntiDeriv.svg for the comparison with the true values of the antiderivative at selected points.");
		Writer(wavelet_ann,a,b,Wish:"AntiDeriv");
	
		Error.WriteLine("\n");
		for(double z = a + 0.1; z <= b; z += 0.1) {Error.WriteLine($"{z}\t{Wavelet_Andreas_afledt(z)}");}
		Writer(gausswavelet_ann,a,b,Wish:"Deriv");
		WriteLine($"\nThe derivative is calculated by a simple weigted sum of the derivative of the function contributions from the single hidden neurons.\nSee plot_Deriv.svg for the comparison with the true values of the derivative at selected points.");
		Writer(wavelet_ann,a,b,Wish:"Deriv");

	}//Main

	public static void Writer(ann TypeAnn, double a, double b, string Wish = "Func")
	{
	if(Wish == "Func")
	{
		Error.WriteLine("\n");
		for(double x = a; x <= b; x+=0.01)
		{
			Error.WriteLine($"{x}\t{TypeAnn.Feed(x)}");
		}
	}
	else if(Wish == "AntiDeriv")
	{
		Error.WriteLine("\n");
		for(double x = a; x <= b; x+=0.01)
		{
			Error.WriteLine($"{x}\t{TypeAnn.AntiDeriv(x)}");
		}
	}
	else if(Wish == "Deriv")
	{
		Error.WriteLine("\n");
		for(double x = a; x <= b; x+=0.01)
		{
			Error.WriteLine($"{x}\t{TypeAnn.Deriv(x)}");
		}
	}
	}		
}//main
