all: A.out B.out C.out particleinbox.svg particleinbox_difference.svg time.txt time.svg time_comparison.svg rotations_comparison.svg 
	echo "\nSee particleinbox.svg for the found solutions.\nSee particleinbox_difference.svg for the difference between the found solutions and the exact solutions.\nSee time.svg for a fit of the calculation time vs matrix size.\nSee time_comparison.svg for a fit of the calculation time vs matrix size for the three methods.\nSee A.out and B.out for a description the answers to the problems.\nThe answer to C is shown in the time_comparison.svg\nAbove are printed fully or partially diagonalized matrices using varying methods" 

A.out particleinbox.dat: mainA.exe
	mono $< 1> A.out 2> particleinbox.dat

B.out time.txt : mainBandC.exe
	echo "Starting diagonalization of multiple large matrices. This takes approx 20 seconds"
	mono $< 2> time.txt 1> B.out

C.out: Makefile
	echo "See rotations_comparison.svg and time_comparison.svg.\nA diagonalized matrix is also printed as the last matrix in B.out for easier comparisonwith the other methods of calculation." > $@
.SILENT:
time.svg: time.txt Makefile
	echo ";\
	set terminal svg ;\
	set output '$@' ;\
	set xlabel 'n' ;\
	set ylabel 'time' ;\
	set key r c ;\
	set logscale y ;\
	u = 100; v = 10; z = 3 ;\
	g(x) = u + (x/v)**z ;\
	set fit quiet ;\
	fit g(x) 'time.txt' i 0 via u,v ;\
	set title 'Elapsed time for diagonalization of nxn matrix' ;\
	plot '$(word 1, $^)' i 0 t 'Elapsed time cyclic', g(x) t sprintf('fit=%.3f+(x/%.3f)^{%.3f}',u,v,z) ;\
	" | gnuplot

rotations_comparison.svg: time.txt Makefile
	echo ";\
	set term svg ;\
	set output '$@' ;\
	set xlabel 'n' ;\
	set ylabel 'Rotations' ;\
	set logscale y ;\
	set title 'Rotations for diagonalization of nxn matrix' ;\
	plot '$(word 1, $^)' i 0 u 1:3 w p t 'Cyclic', '$(word 1, $^)' i 1 u 1:3 w p t 'RowByRow', '$(word 1, $^)' i 2 u 1:3 w p t 'Optimized Classic' ;\
	" | gnuplot

time_comparison.svg: time.txt Makefile
	echo ";\
	set term svg ;\
	set output '$@' ;\
	set xlabel 'n' ;\
	set ylabel 'time' ;\
	set logscale y ;\
	set key r b ;\
	a = 100; b = 10; c = 3 ;\
	d = 100; e = 10; f = 3 ;\
	h = 100; i = 10; j = 3 ;\
	g(x) = a + (x/b)**c ;\
	k(x) = d + (x/e)**f ;\
	l(x) = h + (x/i)**j ;\
	set fit quiet ;\
	fit g(x) '$(word 1, $^)' i 0 via a,b ;\
	fit k(x) '$(word 1, $^)' i 1 via d,e ;\
	fit l(x) '$(word 1, $^)' i 2 via h,i ;\
	set title 'Elapsed time for diagonalization of nxn matrix' ;\
	plot '$(word 1, $^)' i 0 u 1:2 w p t 'Cyclic', '$(word 1, $^)' i 1 u 1:2 w p t 'RowByRow', '$(word 1, $^)' i 2 u 1:2 w p t 'Optimized Classic', g(x) t sprintf('fit=%.2f+(x/%.2f)^{%.2f}',a,b,c), k(x) t sprintf('fit)%.2f+(x/%.2f)^{%.2f}',d,e,f), l(x) t sprintf('fit=%.2f+(x/%.2f)^{%.2f}',h,i,j) ;\
	" | gnuplot

particleinbox.svg: particleinbox.dat Makefile
	echo " ;\
	set term svg ;\
	set output '$@' ;\
	set xlabel 'x/L' ;\
	set ylabel '{/Symbol y}_n(x)' ;\
	set xrange[0:1] ;\
	set yrange[-0.15:0.15] ;\
	set title 'Found wavefunctions' ;\
	plot '$(word 1, $^)' i 0 w l t '{/Symbol y_1}', '$(word 1, $^)' i 1 w l t '{/Symbol y_2}', '$(word 1, $^)' i 2 w l t '{/Symbol y_3}', '$(word 1, $^)' i 3 w l t '{/Symbol y_4}' ;\
	" | gnuplot
	
particleinbox_difference.svg: particleinbox.dat Makefile
	echo " ;\
	set term svg ;\
	set output '$@' ;\
	set xlabel 'x/L' ;\
	set ylabel '{/Symbol y}_n(x)_{exact}-{/Symbol y}_n(x)_{found}' ;\
	set xrange[0:1] ;\
	set yrange[-0.15:0.15] ;\
	set title 'Difference between found wavefunctions and the exact solutions' ;\
	plot '$(word 1, $^)' i 4 w l t '{/Symbol y_1}', '$(word 1, $^)' i 5 w l t '{/Symbol y_2}', '$(word 1, $^)' i 6 w l t '{/Symbol y_3}', '$(word 1, $^)' i 7 w l t '{/Symbol y_4}' ;\
	" | gnuplot

mainA.exe: mainA.cs math.dll
	mcs -reference:$(word 2,$^) -out:$@ $<

mainBandC.exe: mainBandC.cs math.dll
	mcs -reference:$(word 2,$^) -out:$@ $<

math.dll: ../2-LinearEquations/gramschmidt.cs ../../matlib/matrix.cs ../../matlib/vector.cs jacobirowbyrow.cs jacobicyclic.cs jacobiclassic.cs 
	mcs -target:library -out:$@ $^ 

clean: 
	$(RM) *.txt out* *.exe *.pdf *.svg *.dll *.gpi *.log *.gpii *.out
