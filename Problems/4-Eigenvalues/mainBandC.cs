using System;
using static System.Console;
using static System.Math;
using System.Diagnostics;

class main{

	static void Main()
	{
		int points = 8;
		int[] ns = new int[points];
		int[] nsRowByRow = new int[points];
		for(int i = 0;i< points; i++)
		{
			ns[i] = 100+i*10;
			nsRowByRow[i] = ns[i] - 30;
		}
		CalcTimeOrRotations(ns,Method:"Cyclic");
		CalcTimeOrRotations(nsRowByRow,Method:"RowByRow");
		CalcTimeOrRotations(ns,Method:"Classic");

		int n2=7;
		Console.WriteLine($"To see the scaling of the calculation time see time.svg and time_comparison.svg\n");
		Console.WriteLine($"Below are some diagonalized ({n2}x{n2}) (lower triangular) matrices calculated using different methods");
		matrix B = MakeRandomSymmetricMatrix(n2,n2); matrix B1 = B.copy(); 
		B.print("The matrix to be diagonalized:");
	 	//Jacobi_cyclic Diagon_cyclic = new Jacobi_cyclic(B4);
	 	Jacobi_classic Diagon_fast = new Jacobi_classic(B1);
		WriteLine("\n\nDiagonalized matrices:");	
		WriterCyclic("                               Cyclic, #rotations =", B); 
		WriterRowByRow("\n			       Row-by-row, #rotations= ", B);
		WriterCyclic("\nReversed order of eigenvalues, Cyclic,     #rotations= ", B, Reverse:true);
		WriterRowByRow("\nReversed order of eigenvalues, Row-by-Row, #rotations= ", B, Reverse:true);
		WriterRowByRow("\n1 eigenvalue - smallest,       Row-by-Row, #rotations= ", B,  Nume:1);
		WriterRowByRow("\n1 eigenvalue,- largest         Row-by-Row, #rotations= ", B, Reverse:true, Nume:1);

		WriteLine($"            'Largest-off-diagonal' search, #rotations = {Diagon_fast.rotations}");if(B.size2 <= 10){B1.print("");}
	}

	public static void CalcTimeOrRotations(int[] ns, string Method)
	{
		foreach(int n  in ns)
		{
			matrix A = MakeRandomSymmetricMatrix(n,n);
			Stopwatch sw = new Stopwatch();
			if(Method == "Cyclic")
			{
			//Console.WriteLine($"Starting calculation on matrix of size ({n}x{n}) with the Cyclic method");
				sw.Start();
				Jacobi_cyclic diagon = new Jacobi_cyclic(A);
				sw.Stop();
				Error.WriteLine("{0}\t{1}\t{2}",n,sw.ElapsedMilliseconds,diagon.rotations);
			}
			else if(Method == "RowByRow")
			{
			//Console.WriteLine($"Starting calculation on matrix of size ({n}x{n}) with the RowByRow method");
				sw.Start();
				Jacobi_RowByRow diagon = new Jacobi_RowByRow(A);
				sw.Stop();
				Error.WriteLine("{0}\t{1}\t{2}",n,sw.ElapsedMilliseconds,diagon.rotations);
			}
			else if(Method == "Classic")
			{
			//Console.WriteLine($"Starting calculation on matrix of size ({n}x{n}) with the optimized method");
				sw.Start();
				Jacobi_classic diagon = new Jacobi_classic(A);
				sw.Stop();
				Error.WriteLine("{0}\t{1}\t{2}",n,sw.ElapsedMilliseconds,diagon.rotations);
			}
		}
		Error.WriteLine("\n");
	}

	public static void WriterCyclic(string ToPrint, matrix M, bool Reverse = false)
	{
		WriteLine("");
		matrix tmpM = M.copy();
		Jacobi_cyclic Diagon_tmp = new Jacobi_cyclic(tmpM, Reverse);
		WriteLine($"{ToPrint}{Diagon_tmp.rotations}");
		if(M.size2 <= 10) tmpM.print();
	}
	public static void WriterRowByRow(string ToPrint, matrix M, bool Reverse = false, int Nume = 0)
	{
		WriteLine("");
		matrix tmpM = M.copy();
		Jacobi_RowByRow Diagon_tmp = new Jacobi_RowByRow(tmpM, Nume, Reverse);
		WriteLine($"{ToPrint}{Diagon_tmp.rotations}");
		if(M.size2 <= 10) tmpM.print();
	}
	public static void WriterFast(string ToPrint, matrix M, bool Reverse = false)
	{
		WriteLine("");
		matrix tmpM = M.copy();
		Jacobi_classic Diagon_tmp = new Jacobi_classic(tmpM, Reverse);
		WriteLine($"{ToPrint}{Diagon_tmp.rotations}");
		if(M.size2 <= 10) tmpM.print();
	}

	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   	Random random = new Random((int)DateTime.Now.Ticks);
   	return random.NextDouble() * (maximum - minimum) + minimum;
	}

	public static matrix MakeRandomSymmetricMatrix(int cols, int rows)
	{
		matrix SymMatrix = new matrix(cols,rows);
		for(int c = 0; c<cols;c++)
		{
			for(int r = c;  r < rows; r++)
			{
				SymMatrix[c,r] = GetRandomNumber(-10,10);
				SymMatrix[r,c] = SymMatrix[c,r];
			}
		}
		return SymMatrix;
	}
}

