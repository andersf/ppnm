using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		WriteLine("Part A:\n");
		int n=3;
		matrix A = MakeRandomSymmetricMatrix(n,n);
		A.print("Matrix A: ");
		matrix A_old = A.copy();
		Jacobi_cyclic diagon = new Jacobi_cyclic(A);
		diagon.eigenvalues.print("Found eigenvalues: ");
		diagon.V.print("\nV: ");
		
		matrix identity = new matrix(n,n);  identity.set_unity();
		matrix D = new matrix(n,n);
		for(int i =0; i<n; i++) D[i,i] = diagon.eigenvalues[i];
		D.print("\nD: ");
		matrix VTAV = (diagon.V).T*A_old*(diagon.V);
		VTAV.print("\nV^T*A*V: ");
		if(VTAV.approx(D))
			WriteLine("V^T*A*V is approximately equal to the diagonalmatrix of eigenvalues.");
		else 
			WriteLine("V^T*A*V is not close to the diagonalmatrix of eigenvalues.");

		WriteLine("\n\nParticle in a Box");
		WriteLine("See the plots: particleinbox.svg and particleinbox_difference.svg ");
		int nH = 100;
		matrix H = new matrix(nH,nH);
		double s=1.0/(nH+1);
		for(int i=0; i < nH-1; i++)
		{	
			H[i,i]  = -2; 
			H[i,i+1]=  1;
			H[i+1,i]=  1;
		}
		H[nH-1,nH-1] = -2;
		H = -H/s/s;

		Jacobi_cyclic Hdiagon = new Jacobi_cyclic(H);
		
		WriteLine($"{"k",-2}\t{"Calculated",-10}\t{"Exact",-10}\t{"Difference",-10}\t{"Relativ diff",-10}");
		for(int k =0; k<nH/3; k++)
		{
			double exact = PI*PI*(k+1)*(k+1);
			double calculated = Hdiagon.eigenvalues[k];
			WriteLine($"{k,-2}\t{calculated:f8}\t{exact:f8}\t{calculated-exact:f8}\t{(calculated-exact)/exact:f3}");
		}
		
		for(int k=0;k<4;k++)
		{
			Error.WriteLine($"0\t0");
			for(int i=0;i<nH;i++)
			{
				Error.WriteLine($"{(i+1.0)/(nH+1)}\t{Hdiagon.V[i,k]}");
			}
			Error.WriteLine("1\t0\n\n");
		}
		for(int k=0;k<4;k++)
		{
			Error.WriteLine($"0\t0");
			if(Hdiagon.V[0,k]>0 && Sin(PI*(k+1)*(1.0)/(nH+1)) > 0)
			{
				for(int i=0;i<nH;i++)
				{
					Error.WriteLine($"{(i+1.0)/(nH+1)}\t{Hdiagon.V[i,k]-Sqrt(2/Convert.ToDouble(nH))*Sin(PI*(k+1)*(i+1.0)/(nH+1))}");
				}
				Error.WriteLine("1\t0\n\n");
			}
			else
			{
				for(int i=0;i<nH;i++)
				{
					Error.WriteLine($"{(i+1.0)/(nH+1)}\t{Hdiagon.V[i,k]+Sqrt(2/Convert.ToDouble(nH))*Sin(PI*(k+1)*(i+1.0)/(nH+1))}");
				}
				Error.WriteLine("1\t0\n\n");
			}
		}
		
	}

	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   	Random random = new Random((int)DateTime.Now.Ticks);
   	return random.NextDouble() * (maximum - minimum) + minimum;
	}
	
	public static matrix MakeRandomSymmetricMatrix(int cols, int rows)
	{
		matrix SymMatrix = new matrix(cols,rows);
		for(int c = 0; c<cols;c++)
		{
			for(int r = c;  r < rows; r++)
			{
				SymMatrix[c,r] = GetRandomNumber(-10,10);
				SymMatrix[r,c] = SymMatrix[c,r];
			}
		}
		return SymMatrix;
	}
}

