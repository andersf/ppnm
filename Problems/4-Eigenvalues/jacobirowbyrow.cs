using System;
using static System.Math;

public class Jacobi_RowByRow
{
	public readonly vector eigenvalues; 
	public readonly int rotations; 
	public readonly vector f_eigenvalues; 
	public readonly matrix V; //matrix of eigenVectors

	public Jacobi_RowByRow(matrix A, int e = 0, bool Reverse = false){ //Constructor
		rotations = 0;
		int n = A.size1;
		eigenvalues = new vector(n);
		V = new matrix(n,n); V.set_unity();
		vector new_eigenvalues = new vector(e);
		vector old_eigenvalues = new vector(e);
		
		if(e == 0) {e = n;} //If not specified # of eigenvalues all are found. 
		for(int g=0; g<e; g++) //g is Dmitri's p
		{
		do{
		old_eigenvalues = eigenvalues.copy();
		for(int h=g+1; h<n; h++) //h is Dmitri's q
			{	double Agg = A[g,g], Ahh = A[h,h], Agh = A[g,h]; 
				double phi, c, s;
				if(Reverse)
				{phi = Atan2(2*Agh,Agg-Ahh)/2; c = Cos(phi); s=-Sin(phi);}
				else
				{phi = Atan2(2*Agh,Ahh-Agg)/2; c = Cos(phi); s=Sin(phi);}
				rotations ++;
				
				A[g,g] = c*c*Agg - 2*s*c*Agh + s*s*Ahh;
				A[h,h] = s*s*Agg + 2*s*c*Agh + c*c*Ahh;
				A[g,h] = s*c*(Agg-Ahh) + (c*c-s*s)*Agh;
				
				//if(Method == "Cyclic") //If the full cyclic method is wanted this part is used. 
				//{
				//	for(int i=0; i<g ; i++)
				//	{	double Aig = A[i,g], Aih = A[i,h];
				//		A[i,g] = c*Aig-s*Aih;
				//		A[i,h] = s*Aig+c*Aih;
				//	}
				//}

				for(int i=g+1; i<h ; i++)
				{	double Agi = A[g,i], Aih = A[i,h];
					A[g,i] = c*Agi-s*Aih;
					A[i,h] = s*Agi+c*Aih;
				}	
				for(int i=h+1; i<n; i++)
				{	double Agi = A[g,i], Ahi = A[h,i];
					A[g,i] = c*Agi-s*Ahi;
					A[h,i] = s*Agi+c*Ahi;
				}		
				
				for(int i=0; i<n; i++)
				{	double Vig = V[i,g], Vih = V[i,h];
					V[i,g] = c*Vig - s*Vih;
					V[i,h] = s*Vig + c*Vih;
					eigenvalues[i] = A[i,i];
				}
			}
		new_eigenvalues = eigenvalues.copy();
		}while(!vector.approx(old_eigenvalues,new_eigenvalues));	
			}
		f_eigenvalues = new vector(e);
		for(int i = 0; i<e; i++){f_eigenvalues[i] = eigenvalues[i];}
	}
}

