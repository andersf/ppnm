using static System.Diagnostics.Debug;
using System.Linq;

public class CSpline
{
	
	double[] x,y,p,c,dx,b,d,D,Q,B;

	public CSpline(double[] xs, double[] ys)
	{
		int n = xs.Length;
		x  = new double[n];
		y  = new double[n];
		b  = new double[n];
		p  = new double[n-1];
		c  = new double[n-1];
		dx = new double[n-1];
		d  = new double[n-1];
		D  = new double[n];
		Q  = new double[n-1];
		B  = new double[n];
		for(int i=0; i<n;i++){x[i]=xs[i]; y[i]=ys[i];}
		for(int i=0; i<n-1;i++){dx[i] = x[i+1]-x[i]; p[i]=(y[i+1]-y[i])/dx[i];}
		
		D[0] = 2; D[n-1]=2; 
		Q[0]=1; 
		B[0] = 3*p[0]; B[n-1]=3*p[n-2];
		for(int i=0; i<n-2;i++)
		{
			D[i+1] = 2*dx[i]/dx[i+1]+2; 
			Q[i+1] = dx[i]/dx[i+1]; 
			B[i+1] = 3*(p[i]+p[i+1]*dx[i]/dx[i+1]);
		}
		for(int i=1; i<n;i++)
		{
			D[i] -= Q[i-1]/D[i-1]; 
			B[i] -= B[i-1]/D[i-1];
		}	

		b[n-1]=B[n-1]/D[n-1];
		for(int i=n-2;i>=0;i--)
		{
			b[i] = (B[i]-Q[i]*b[i+1])/D[i];
		}

		for(int i=0;i<n-1;i++)
		{
			c[i] = (-2*b[i]-b[i+1]+3*p[i])/dx[i];
			d[i] = (b[i]+b[i+1]-2*p[i])/(dx[i]*dx[i]);
		}
	}

	public double Cinterp(double z)
	{
	//x and y are arrays of the data we wish to interpolate
	//z is the given point we with to calculate
	//binary search finds the internal [x[i],x[i+1]] where z is.
	//afterward the linear interpolation finds the corresponding y-value of the z-point. 
	int i = WorkHorses.BinarySearch(x,z);
	double dz = z-x[i];
	return y[i]+dz*(b[i]+dz*(c[i]+dz*d[i]));
	}
	
	public double CinterpInteg(double z)
	//Calculates the integral from the lowest x to the point z.
	{
		double area = 0; int i=0;
		int xlast = WorkHorses.BinarySearch(x,z);
		while(i<xlast)
		{
			area += dx[i]*(y[i]+dx[i]*(b[i]/2+dx[i]*(c[i]/3+dx[i]*d[i]/4)));
			i++;
		}
		double dz = z-x[i];
		area += dz*(y[i]+dz*(b[i]/2+dz*(c[i]/3+dz*d[i]/4)));
		return area;
	}
	
	public double CinterpDeriv(double z)
	{
		int i = WorkHorses.BinarySearch(x,z);
		return b[i] + 2*c[i]*(z-x[i]) + 3*d[i]*(z-x[i])*(z-x[i]);
	}

}
