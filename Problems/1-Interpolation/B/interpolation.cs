using static System.Diagnostics.Debug;
using System.Linq;

public class QSpline
{
	
	double[] x,y,p,c,dx,b;

	public QSpline(double[] xs, double[] ys)
	{
		int n = xs.Count();
		x  = new double[n];
		y  = new double[n];
		b  = new double[n-1];
		p  = new double[n-1];
		c  = new double[n-1];c[0] = 0;
		dx = new double[n-1];
		for(int i=0; i<n;i++){x[i]=xs[i]; y[i]=ys[i];}
		for(int i=0; i<n-1;i++)
		{
			dx[i] = x[i+1]-x[i]; p[i]=(y[i+1]-y[i])/dx[i]; Assert(dx[i]>0);
		}
		for(int i=0; i<n-2;i++)
		{
			c[i+1] = (p[i+1]-p[i]-c[i]*dx[i])/dx[i+1];
		}
		c[n-2] /= 2;
		for(int i=n-3;i>=0;i--)
		{
			c[i] = (p[i+1]-p[i]-c[i+1]*dx[i+1])/dx[i];
		}
		for(int i=0;i<=n-2;i++)
		{
			b[i] = p[i]-c[i]*dx[i];
		}

	}

	public double Qinterp(double z)
	{
	//x and y are arrays of the data we wish to interpolate
	//z is the given point we with to calculate
	//binary search finds the internal [x[i],x[i+1]] where z is.
	//afterward the linear interpolation finds the corresponding y-value of the z-point. 
	int i = WorkHorses.BinarySearch(x,z);
	double dz = z-x[i];
	return y[i]+b[i]*dz+c[i]*dz*dz;
	}
	
	public double QinterpInteg(double z)
	//Calculates the integral from the lowest x to the point z.
	{
		double area = 0; int i=0;
		int xlast = WorkHorses.BinarySearch(x,z);
		while(i<xlast)
		{
			area += y[i]*dx[i] + 0.5*b[i]*dx[i]*dx[i] + c[i]*dx[i]*dx[i]*dx[i]/3;//calculation of area
			i++;
		}
		double dz = z-x[i];
		area += y[i]*dz + 0.5*b[i]*dz*dz + c[i]*dz*dz*dz/3;//calculation of area
		return area;
	}
	
	public double QinterpDeriv(double z)
	{
		int i = WorkHorses.BinarySearch(x,z);
		return b[i] + 2*c[i]*(z-x[i]);
	}

}
