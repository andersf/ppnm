using System;
using System.IO;
using System.Linq;
class main
{
	static void Main()
	{
		var data = WorkHorses.ReadFromFile("diffgauss_table.txt");
		int n = 1000; 
		QSpline diffgauss = new QSpline(data.Item1,data.Item2);

		double z, exact_integ, exact, approx_integ, approx, exact_deriv, approx_deriv;
		double step=(data.Item1.Last()-data.Item1[0])/(n-1);  
		for(int i = 0; i<n; i++)
		{
			z = data.Item1[0]+step*i;
			exact = -2*z*Math.Exp(-z*z);
			approx = diffgauss.Qinterp(z);
			exact_integ = Math.Exp(-z*z);
			approx_integ = diffgauss.QinterpInteg(z);
			exact_deriv = -2*Math.Exp(-z*z)*(1-2*z*z);
			approx_deriv = diffgauss.QinterpDeriv(z);
			Console.Error.WriteLine($"{z:f6}\t{exact:f6}\t{approx:f6}\t{exact_integ:f6}\t{approx_integ:f6}\t{exact_deriv:f6}\t{approx_deriv:f6}");

		}
		double z2 = 2;
		double area = diffgauss.QinterpInteg(z2);
		Console.WriteLine($"The area of 2xexp(-x^2) from x= {data.Item1[0]} to x = {z2} is {area} -- The area should be close to 0");
		Console.WriteLine($"Calculated with quadratic spline");
		Console.WriteLine($"\nPlots that prove that the spline and integrator works as intended:\nqinterp_diffgauss.svg, qinterp_diffgauss_deriv.svg and qinterp_diffgauss_integral.svg");

	}
}


