using static System.Diagnostics.Debug;
using System.Linq;

public class LinearSpline
{
	
	double[] x,y,a1;

	public LinearSpline(double[] xs, double[] ys)
	{
		int n = xs.Count();
		x  = new double[n];
		y  = new double[n];
		a1 = new double[n-1];
		for(int i=0; i<n;i++){x[i]=xs[i]; y[i]=ys[i];}		
		for(int i=0; i<n-1;i++){a1[i]=(y[i+1]-y[i])/(x[i+1]-x[i]);Assert((x[i+1]-x[i])>0);}
	}

	public double Linterp(double z)
	{
	//x and y are arrays of the data we wish to interpolate
	//z is the given point we with to calculate
	//binary search finds the interval [x[i],x[i+1]] where z is.
	//afterward the linear interpolation finds the corresponding y-value of the z-point. 
	int i = WorkHorses.BinarySearch(x,z);
	double dx = z-x[i];
	return y[i]+a1[i]*dx;
	}
	
	public double LinterpInteg(double z)
	//Calculates the integral from the lowest x to the point z.
	{
		double area = 0; int i=0;
		int xlast = WorkHorses.BinarySearch(x,z);
		while(i<xlast)
		{
			double dx = x[i+1]-x[i]; double dy = y[i+1] - y[i]; //definitions to simplify next line of code
			area += y[i]*dx + 0.5*dy*dx; //calculation of area
			i++;
		}
		double dz = z-x[i];
		double Linterp_z = y[i]+a1[i]*dz;
		area += y[i]*dz+0.5*(Linterp_z-y[i])*dz;
	return area;
	}
}
