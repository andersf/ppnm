using System;
using System.IO;
using System.Linq;
class main
{
	static void Main()
	{
		var data = WorkHorses.ReadFromFile("diffgauss_table.txt");
		LinearSpline diffgauss = new LinearSpline(data.Item1,data.Item2);

		for(int i = 0; i < data.Item1.Length; i++)
			Console.Error.WriteLine($"{data.Item1[i]}\t{Math.Exp(-data.Item1[i]*data.Item1[i])}");
		
		Console.Error.WriteLine("\n");
		int n = 1000;
		double x;
		double intervalsize = (data.Item1.Max() - data.Item1.Min())/(n-1);
		for(int i = 0; i < n; i++)
		{
			x = data.Item1.Min()+intervalsize*i;
			Console.Error.WriteLine($"{x}\t{ diffgauss.Linterp(x)}\t{diffgauss.LinterpInteg(x)}");
		}
		
		double z = 2;
		double area = diffgauss.LinterpInteg(z);
		Console.WriteLine($"The area of 2xexp(-x^2) from x = {data.Item1[0]} to x = {z} is {area} -- should be close to 0");
		Console.WriteLine($"Calculated with linear spline");
		Console.WriteLine($"\nPlots that prove that the spline and integrator works as intended:\nlinterp_diffgauss.svg and integral_linterp.svg");
	}
}


