using System;
using System.Collections.Generic;
using static System.Diagnostics.Debug;
using System.Linq;
using System.IO;
using static System.Random;
public class WorkHorses
{
	public static int BinarySearch(double[] x, double z)
	// find the i that fulfills the criteria that x[i] < z
	// Binary search
	{
		int i=0; int j = x.Length-1;
		Assert(j>1-1 && z>=x[0] && z<x[j]);
		while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;}
		return i;
	}

	public static Tuple<double[], double[]> ReadFromFile(string inputfile)
	//Function to read input files. Copied code from ../5-io/read-file.cs
	{
		
		StreamReader instream = new StreamReader(inputfile);
		int lc = File.ReadLines(inputfile).Count();
		double[] xvalues = new double[lc];
		double[] yvalues = new double[lc];
		
		int i = 0;
		string line;
		while((line = instream.ReadLine()) != null)
		{
			string[] data = line.Split(',','\t');
			xvalues[i] = double.Parse(data[0]);
			yvalues[i] = double.Parse(data[1]);
			i++;
		}
		instream.Close();
		return Tuple.Create(xvalues,yvalues);
	}
}
