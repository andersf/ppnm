using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		WriteLine("Part B:");
		WriteLine("Calculating the error of Sin(x*y*z) as a function of # of points.");
		WriteLine("See B.svg for the fit");
		Func<vector,double> error_test = (x) => Sin(x[0]*x[1]*x[2]);
		vector a_error = new vector(0,0,0), b_error = new vector(PI,PI,PI);
		integrator errors;
		for(int N_error = 100; N_error <= 10000; N_error+=100)
		{
			errors = new integrator(error_test,a_error,b_error,N_error,"plainmc");
			Error.WriteLine($"{N_error}\t{errors.Sigma}");
		}		
		
	}//Main


}//main
