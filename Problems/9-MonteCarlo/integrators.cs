using System;
using static System.Math;

public partial class integrator
{
	private double result, sigma;
	public double Result{get{return result;}}
	public double Sigma{get{return sigma;}}
	public int Counter{get{return counter;}}
	private Func<vector,double> f;
	private int counter, N;	

	public integrator(Func<vector,double> F, vector a, vector b, int n, string Method, double acc = 1e-2, double eps = 1e-2)
	{
		this.f = (v) => {counter++;return F(v);}; this.N = n; double Acc = acc, Eps = eps; 
		counter = 0;
		if(Method == "plainmc") plainmc(a,b,N);
		else if(Method == "strata") strata(a,b, Acc, Eps, N);
	}//constructor	

	public double[] strata(vector a, vector b, double Acc, double Eps, int N)
	{	
		double[] final = plainmc(a,b,N);
		this.counter++;
		if(final[1] >= Acc+Eps*Abs(final[0]))
		{
		int hi = HighestSubVar(a,b);
		vector b1 = b.copy(); b1[hi] = (b[hi]+a[hi])/2;
		vector a2 = a.copy(); a2[hi] = b1[hi];

		double[] strata_L = strata(a,b1,Acc/Sqrt(2.0),Eps,N), strata_U = strata(a2,b,Acc/Sqrt(2.0),Eps,N);
		final = new double[]{strata_L[0] + strata_U[0],Sqrt(Pow(strata_L[1],2) + Pow(strata_U[1],2))};
		}
		result = final[0]; sigma = final[1];
		return final;
	}//strata

	public double[] plainmc(vector a, vector b, int N)
	{
		double V = 1;
		for(int i = 0; i < a.size; i++)
		{
			V *= b[i]-a[i];
		}//for
		
		double sum1 = 0, sum2 = 0, fx;
		vector x = new double[a.size];
		for(int i = 0; i < N; i++)
		{
			x     = randomx(a,b);
			fx    = f(x);
			sum1 += fx;
			sum2 += fx*fx;
		}//for
		
		double mean = sum1/N;
		result = mean*V;
		sigma = V*Sqrt((sum2/N-mean*mean)/N); //from theory example.
		double[] final = new double[]{result,sigma};
		return final;
	}//plainmc

	public vector randomx(vector a, vector b)
	{
		vector x = new vector(a.size);
		Random rand = new Random((int)DateTime.Now.Ticks);
		for(int i = 0; i < a.size; i++)
		{
			x[i] = a[i] + rand.NextDouble()*(b[i]-a[i]);
		}//for
		return x;
	}//randomx

	public int HighestSubVar(vector a, vector b)
	{
			int hi = 0; vector b1 = b.copy(); vector a2 = a.copy();
			double[] tmp_plainmc_L = new double[2];
			double[] tmp_plainmc_U = new double[2];
			double[] sub_variances = new double[a.size*2];
			for(int i = 0; i < a.size; i++)
			{
				b1[i] = (b[i]+a[i])/2;
				a2[i] = b1[i]; 
				tmp_plainmc_L = plainmc(a,b1,N);
				tmp_plainmc_U = plainmc(a2,b,N);		
			       	sub_variances[2*i] = tmp_plainmc_L[1];
				sub_variances[2*i+1] = tmp_plainmc_U[1];
				if(sub_variances[2*i] > sub_variances[hi])
					hi = 2*i;
				else if(sub_variances[2*i+1] > sub_variances[hi])
					hi = 2*i+1;
				a2[i] = a[i]; b1[i] = b[i];
			}//for
			return hi /= 2;	
	}//HighestSubVar

	public void Writer(string function, int N, double TrueVal)
	{
		Console.Error.WriteLine($"Integration of {function} with {N} points.");
		Console.Error.WriteLine($"Integral  = {result:f6}");
		Console.Error.WriteLine($"Sigma     = {sigma:f6}");
		Console.Error.WriteLine($"True value= {TrueVal:f6}");
		Console.Error.WriteLine($"Error     = {Abs(result-TrueVal):f6} = {Abs(result-TrueVal)/sigma:f6} sigma\n");

	}//Writer
}//minimizer 
