using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		WriteLine("Part C:");
		WriteLine("Due to the random nature of the points the duration of the calculations may vary a lot and can thus take a little while.");
		
		WriteLine("Calculating the integral of x*x+y*y from (-2,-2) to (2,2):");
		Func<vector,double> circle = (x) => x[0]*x[0]+x[1]*x[1];	
		vector a = new vector(-2,-2); vector b = new vector(2,2); int N = 2000;
		integrator Circle = new integrator(circle,a,b,N, "strata");
		Circle.Writer("x^2*y^2", Circle.Counter, TrueVal:42.666666);
		
		WriteLine("Calculating the integral of (1-cos(x)cos(y)cos(z))^-1 from (0,0,0) to (PI,PI,PI): ");
		Func<vector,double> A_func = (x) => 1/(1-Cos(x[0])*Cos(x[1])*Cos(x[2]))/PI/PI/PI;
		vector a_A = new vector(0,0,0), b_A = new vector(PI,PI,PI);
		int N_A = 70000;
		integrator exercise = new integrator(A_func,a_A,b_A,N_A, Method:"strata",acc:8e-3,eps:8e-3);
		exercise.Writer("Dmitri's singular integral", exercise.Counter, TrueVal:1.39320392968);
	

		bool IncludeMe = true;
		if(IncludeMe)
		{
		WriteLine("Calculating the integral of (Sqrt(x*y*z))^-1 from (0,0,0) to (1,1,1): ");
		Func<vector,double> eor = (r) => 1/Sqrt(r[0]*r[1]*r[2]);
		vector a_eor = new vector(0,0,0), b_eor = new vector(1,1,1);
		int N_eor = 100000;
		integrator eorint = new integrator(eor,a_eor,b_eor,N_eor,Method:"strata",acc:1e-2,eps:1e-2);
		eorint.Writer("(Sqrt(x*y*z))^-1", eorint.Counter, TrueVal:8.0);
		}
	}//Main


}//main
