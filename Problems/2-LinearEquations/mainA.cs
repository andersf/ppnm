using System;
using static System.Console;
class main{
	static void Main()
	{
		int n=4,m=3;
		matrix A1 = new matrix(n,m);
		for(int i = 0; i<n;i++)
		{
			for(int j = 0; j < m; j++)
			{
				A1[i,j] = GetRandomNumber(-10,10);
			}	
		}
		A1.print($"Qr-decomposition: \n random {n}x{m} (tall) matrix A:");
		GramSchmidt QRA = new GramSchmidt(A1);
		matrix Q = QRA.Q;
		matrix R = QRA.R;
		R.print("Matrix R:");
		bool Ris0 = true;
		for(int j = 0; j<R.size2; j++)
		{
			for(int i = j+1; i<R.size2; i++)
			{
				if(!cmath.approx(R[i,j],0.0)) Ris0 = false;
			}
		}
		WriteLine("'R is upper triangular' is {0}",Ris0);
		matrix I1 = new matrix(Q.size2,Q.size2); I1.setid();  
		matrix QTQ = Q.T*Q; QTQ.print("Q^T*Q:");
		WriteLine("Q^T*Q = I is {0}",QTQ.approx(I1));
		matrix QR = Q*R; QR.print("Q*R:");
		WriteLine("Q*R = A is {0}",QR.approx(A1));
	
		
	
	
		int n2=4;
		matrix A2 = new matrix(n2,n2);
		vector b = new vector(n);
		for(int i = 0; i<n;i++)
		{
			for(int j = 0; j < n; j++)
			{
				A2[i,j] = GetRandomNumber(-10,10);
			}
			b[i] = GetRandomNumber(-10,10);
		}
		A2.print($"\nQr-decomposition: \n random {n}x{n} (Square) matrix A:");
		var QRA2 = new GramSchmidt(A2);
		var Q2 = QRA2.Q;
		var R2 = QRA2.R;
		matrix Q2R2 = Q2*R2; Q2R2.print("Q*R:");
		if(A2.approx(Q2*R2)){WriteLine("Q*R is a good approximation to matrix A");}
		else{WriteLine("Q*R is a bad approximation to matrix A");}
		
		b.print("vector b:\n");
		vector x = QRA2.qr_gs_solve(b);
		x.print("vector x:\n");
		vector A2x = A2*x; A2x.print("A*x:");
		if((A2*x).approx(b)){WriteLine("A*x is a good approximation to vector b");}
		else{WriteLine("A*x is a bad approximation to vector b");}
	}
	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   		Random random = new Random((int)DateTime.Now.Ticks);
 	  	return random.NextDouble() * (maximum - minimum) + minimum;
	}	

}
