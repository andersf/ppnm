using System;
using static System.Console;
class main{
	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   	Random random = new Random((int)DateTime.Now.Ticks);
   	return random.NextDouble() * (maximum - minimum) + minimum;
	}	
	static void Main()
	{
		int n=4,m=4;
		matrix A = new matrix(n,m);
		vector b = new vector(n);
		for(int i = 0; i<n;i++)
		{
			for(int j = 0; j < m; j++)
			{
				A[i,j] = GetRandomNumber(-10,10);
			}
			b[i] = GetRandomNumber(-10,10);
		}
		A.print($"Qr-decomposition: \n random {n}x{m} matrix A:");
		var QRA = new GivensRotation(A);
		
		vector x = QRA.Givens_solve(b);
		vector Ax = A*x; Ax.print("A*x:\n");
		b.print("vector b:\n");
		if((A*x).approx(b)){WriteLine("A*x is a good approximation to vector b");}
		else{WriteLine("A*x is a bad approximation to vector b");}
	}
}
