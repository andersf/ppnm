using System;
using static System.Math;

public class GramSchmidt 
{
	public readonly matrix Q,R;
	public GramSchmidt(matrix A)
	{	
		Q = A.copy(); int n = Q.size2;
		R = new matrix(n,n);
		for(int i = 0; i < n; i++)
		{
			R[i,i] = Sqrt(Q[i]%Q[i]);
			Q[i] /= R[i,i];
			for(int j = i+1; j < n; j++)
			{
				R[i,j] = Q[i]%Q[j];
				Q[j] -= Q[i]*R[i,j];
			}

		}
	}
	public vector qr_gs_solve(vector p)
	{
		//vector x = new vector(b.size);
		vector c = Q.transpose()*p;
		backsub(c);
		return c;
	}

	public void backsub(vector b)
	{
		//vector c = new vector(n);
		for(int i = b.size-1; i>=0; i--)
		{
			for(int k = i+1; k < b.size; k++)
			{
				b[i] -= R[i,k]*b[k];
			}
			b[i] /= R[i,i];
		}
	}
	public matrix inverse()
	{
		matrix Ainv = new matrix(R.size1,R.size1);
		matrix identity = new matrix(R.size1,R.size1); identity.set_identity();
		for(int i = 0; i<R.size1; i++)
		{
			Ainv[i] = qr_gs_solve(identity[i]);		
		}
	return Ainv;
	}
}
