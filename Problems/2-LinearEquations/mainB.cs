using System;
using static System.Console;
class main{
	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   	Random random = new Random((int)DateTime.Now.Ticks);
   	return random.NextDouble() * (maximum - minimum) + minimum;
	}	
	static void Main()
	{
	
		int n=4;
		matrix A = new matrix(n,n);
		for(int i = 0; i<n;i++)
		{
			for(int j = 0; j < n; j++)
			{
				A[i,j] = GetRandomNumber(-10,10);
			}
		}
		A.print($"Qr-decomposition: \n random {n}x{n} matrix A:");
		var QRA = new GramSchmidt(A);
		var Q = QRA.Q;
		var R = QRA.R;
		R.print("Matrix R:");
		matrix QR = Q*R; QR.print("Q*R:");
		//test if Q*R is approx A)
		if(A.approx(Q*R)){WriteLine("Q*R is a good approximation to matrix A");}
		else{WriteLine("Q*R is a bad approximation to matrix A");}
		
		matrix Ainv = QRA.inverse();
		matrix Identity = new matrix(n,n); Identity.set_unity();
		Ainv.print("\nThe inverse matrix of A:");
		if((A*Ainv).approx(Identity)){WriteLine("A*A⁻¹ is approximately equal to the identity matrix.");}
		else{WriteLine("A*A⁻¹ is not equal to the identity matrix.");}
		if((Ainv*A).approx(Identity)){WriteLine("A⁻¹*A is approximately equal to the identity matrix.");}
		else{WriteLine("A⁻¹*A is not equal to the identity matrix.");}
		matrix A1A = Ainv*A; matrix AA1 = A*Ainv;
		A1A.print("\nBelow, these matrix products are printed\nA⁻¹*A"); 
		AA1.print("\nA*A⁻¹");	
	}

}
