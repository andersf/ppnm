using System;
using static System.Math;

public class GivensRotation 
{
	public readonly matrix QR;
	public GivensRotation(matrix A)
	{
		QR = A.copy();
		for(int p = 0; p<QR.size2;p++)
		{
			for(int q = p+1; q<QR.size1; q++)
			{
				double theta = Atan2(QR[q,p],QR[p,p]);
				for(int k=p;k<QR.size2;k++)
				{
					double QRpk = QR[p,k], QRqk = QR[q,k];
					QR[p,k] = QRpk*Cos(theta)+QRqk*Sin(theta);
					QR[q,k] = -QRpk*Sin(theta)+QRqk*Cos(theta);
				}
				QR[q,p] = theta;
			}
		}
	}
	public vector Givens_solve(vector b)
	{
		vector d = b.copy();
		d = GivensRotation_QTvec(d);		
		d = Givens_backsub(d);
	return d;	
	}

	public vector GivensRotation_QTvec(vector Gb)
	{
		vector c = Gb.copy();
		for(int p = 0; p<QR.size2; p++)
		{
			for(int q = p+1; q<QR.size1; q++)
			{
				double cp = c[p], cq=c[q], theta = QR[q,p];
				c[p] = cp*Cos(theta)+cq*Sin(theta);
				c[q] = -cp*Sin(theta)+cq*Cos(theta);
			}
		}
	return c;
	}

	public vector Givens_backsub(vector c)
	{
		vector e = c.copy();
		for(int i = QR.size2-1; i>=0; i--)
		{
			for(int k = i+1; k < QR.size2; k++)
			{
				e[i] -= QR[i,k]*e[k];
			}
			e[i] /= QR[i,i];
		}
	return e;
	}
	
	public matrix inverse()
	{
		matrix Ainv = new matrix(QR.size1,QR.size1);
		matrix identity = new matrix(QR.size1,QR.size1); identity.set_identity();
		for(int i = 0; i<QR.size1; i++)
		{
			Ainv[i] = Givens_solve(identity[i]);		
		}
	return Ainv;
	}
}
