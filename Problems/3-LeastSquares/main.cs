using System;
using static System.Console;
using System.IO;
class main{
	static void Main()
	{
		var data = LeastSquares.ReadFromFile("input.txt");
		vector uncertainty = new vector(data.Item2.size);
		vector unc_plot = new vector(data.Item2.size);
			for(int i =0; i<data.Item2.size; i++){
				uncertainty[i] = 0.05;
				unc_plot[i] = data.Item2[i]/20;
			}
		
		vector logb = new vector(data.Item2.size);
		for(int i =0; i<data.Item2.size; i++) logb[i] = Math.Log(data.Item2[i]);
		Func<double,double>[] func = new Func<double,double>[]{t=>1,t=>-t};
		LeastSquares exp_dec = new LeastSquares(data.Item1,logb,uncertainty,func);
		
		Error.WriteLine("\n");
		for(int i = 0; i<data.Item1.size;i++)
			{Error.WriteLine("{0}\t{1}\t{2}", data.Item1[i], data.Item2[i], unc_plot[i]);}
		
		double halflife = Math.Log(2)/exp_dec.fit[1];
		double mod_hl   = 3.63;
		WriteLine("####### Exercise A #######");
		WriteLine("The calculated half-life is: {0,5:g5} days",halflife);
		WriteLine("The modern experimentally determined half-life of 224Ra is {0} days.\nThe half-life calculated from Rutherford and Soddy's experiments is {1,4:g4} percent away from the modern result.",mod_hl,(halflife/3.63-1)*100);
		
		WriteLine("\n\n####### Exercise B #######");
		Error.WriteLine("\n");
		exp_dec.Covariance.print("Covariance:",stream:"Error");
		//WriteLine("Uncertainty of the activity at time 0: {0,4:g4}",Math.Exp(Math.Sqrt(exp_dec.Covariance[0,0])));
		//WriteLine("Uncertainty of the exponential constant: {0,4:g4}\n\n",Math.Sqrt(exp_dec.Covariance[1,1]));
		
		Error.WriteLine("\n");
		Error.WriteLine("{0}\t{1}",Math.Exp(Math.Sqrt(exp_dec.Covariance[0,0])),Math.Sqrt(exp_dec.Covariance[1,1]));

		double dhalflife = Math.Abs(-Math.Log(2.0)/exp_dec.fit[1]/exp_dec.fit[1] * Math.Sqrt(exp_dec.Covariance[1,1]));
		WriteLine("The half-life determined by Rutherford and Soddy is {0,6:g6} +/- {1,6:g6}",halflife,dhalflife);
		if(mod_hl <= halflife+dhalflife && mod_hl >= halflife-dhalflife)
			WriteLine("The modern result of 224Ra's half-life is within the experimental uncertainties of Rutherford and Soddy's experiments.\n\n");
		else
			WriteLine("The modern result of 224Ra's half-life is NOT in the interval of the upper and lower uncertainty limit of RutherFord and Soddy's result.\n\n");
		WriteLine("####### Exercise C #######\n See plot.svg.");
	}

	public static double GetRandomNumber(double minimum, double maximum)
	{ 
   	Random random = new Random((int)DateTime.Now.Ticks);
   	return random.NextDouble() * (maximum - minimum) + minimum;
	}	
}
