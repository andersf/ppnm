using System;
using static System.Math;
using System.IO;
using System.Linq;

public class LeastSquares
{
	public vector fit;
	public matrix Covariance;
	public matrix A;
	public LeastSquares(vector xs, vector ys, vector dys, Func<double,double>[] fs)
	{
		vector x = xs.copy();
		vector dy = dys.copy();
		vector b = ys.copy();		
		A = new matrix(x.size,fs.Length);
		for(int i =0;i<b.size;i++)
			{
			b[i] = b[i]/dy[i];
			for(int k = 0; k<fs.Length;k++)
				{
				A[i,k] = fs[k](x[i])/dy[i];
				}
			}
		GramSchmidt QRfit = new GramSchmidt(A);
	
		fit = QRfit.qr_gs_solve(b);
		fit.print("",stream:"Error");

		GramSchmidt Sigma = new GramSchmidt(A.T*A);
		Covariance = new matrix(fs.Length,fs.Length);
		Covariance = Sigma.inverse();

	}
	
	public static Tuple<vector, vector> ReadFromFile(string inputfile)
	//Function to read input files. Copied code from ../5-io/read-file.cs
	{
		
		StreamReader instream = new StreamReader(inputfile);
		int lc = File.ReadLines(inputfile).Count();
		vector xvalues = new vector(lc);
		vector yvalues = new vector(lc);
		
		int i = 0;
		string line;
		while((line = instream.ReadLine()) != null)
		{
			string[] data = line.Split(',','\t');
			xvalues[i] = double.Parse(data[0]);
			yvalues[i] = double.Parse(data[1]);
			i++;
		}
		instream.Close();
		return Tuple.Create(xvalues,yvalues);
	}
}		
