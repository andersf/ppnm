using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
class main{

	static void Main()
	{
		Func<double,vector,vector> f = (t,y) => new vector(y[9]/m1, y[10]/m1, y[11]/m1, y[12]/m2, y[13]/m2, y[14]/m2, y[15]/m3, y[16]/m3, y[17]/m3, 
		3/2*Pow(m1*m2,2)/Pow([0]-[2],5/2)-3/2*Pow(m1*m3,2)/Pow([4]-[0],5/2), 
		3/2*Pow(m1*m2,2)/Pow([1]-[3],5/2)-3/2*Pow(m1*m3,2)/Pow([5]-[1],5/2), 
	  -3/2*Pow(m2*m1,2)/Pow([0]-[2],5/2)-3/2*Pow(m2*m3,2)/Pow([4]-[2],5/2), 
	  -3/2*Pow(m2*m1,2)/Pow([1]-[3],5/2)-3/2*Pow(m2*m3,2)/Pow([5]-[3],5/2), 
		3/2*Pow(m3*m2,2)/Pow([4]+[2],5/2)+3/2*Pow(m3*m1,2)/Pow([4]-[0],5/2), 
		3/2*Pow(m3*m2,2)/Pow([5]-[3],5/2)+3/2*Pow(m3*m1,2)/Pow([5]-[1],5/2), 
		m1 = 1;
		m2 = 1;
		m3 = 1;
		
		vector y_start = new vector(	0.97000436 , -0.24308753, 0.93240737/2, 86473146/2, 
						-0.97000436,  0.24308753, 0.93240737/2, 86473146/2, 
						0,        0, -0.93240737, -86473146);
		
		double start = 0.0, end = 1000;  	
		double acc = 1e-5, eps = 1e-5;

		rkstepXY SinCos = new rkstepXY(f,start,end,y_start, acc,eps);
		List<double> xs = SinCos.xValues; 
		List<vector> ys = SinCos.yValues; 
		List<vector> errs = SinCos.Errors; 
		for(int i = 0; i<xs.Count; i++){WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",xs[i],ys[i][0],ys[i][1],errs[i][0],errs[i][1]);}
		Error.WriteLine("Information from the calculation of the differential equation u''=-u:");
		Error.WriteLine("acc = {0}, eps = {1}",acc,eps);
		Error.WriteLine($"a = {start}, b = {end}, y0(a) = {y_start[0]}, y1(a) = {y_start[1]}");
		Error.WriteLine($"y0(b) = {ys[ys.Count-1][0]}, y1(b) = {ys[ys.Count-1][1]}");
		Error.WriteLine($"sin(b) = {Sin(end)}, cos(b) = {Cos(end)}");
		if(CloseEnough(ys[ys.Count-1][0],Sin(end),acc) && CloseEnough(ys[ys.Count-1][1],Cos(end),acc))
			Error.WriteLine("Test passed. The method has succesfully found the known solution to the differential equation");
		else
			Error.WriteLine("Test failed");
	}

	static bool CloseEnough(double trialvalue, double goal, double acc)
	{	
		if(Abs(trialvalue - goal) <= acc)
			return true;
		else 
		{
			Error.WriteLine("Trialvalue = {0}, goal = {1}, acc = {2}",trialvalue,goal,acc);
			return false;
		}		
	}
}

