using System;
using static System.Math;
using System.Linq;
using System.Collections.Generic;
public class rkstepXY 
{
	private Func<double,vector,vector> f; 
	private double a, b;
	private vector err, y0;
	private double eps, acc; 
	public List<double> xValues;
	public List<vector> yValues, Errors;



	public rkstepXY(Func<double,vector,vector> func, double start, double end, vector ya, double eps0, double acc0)
	{ //Constructor
		f = func;
		a = start; b = end; eps = eps0; acc = acc0;
		y0 = ya.copy();
		//size = ya.size; //order of polynomial
		Tuple<List<double>, List<vector>, List<vector>> results = Driver(y0);
		xValues = results.Item1;
		yValues = results.Item2;
		Errors  = results.Item3;
	}
	
	public vector[] Stepper(vector yn, double h, double xn)
	{	//Advances solution one step
		//Estimates the local error
		vector ynplus_lo = yn.copy(); vector ynplus_up = yn.copy();
		vector bvals  = new vector(4); bvals[0] = 2.0/9; bvals[1] = 1.0/3; bvals[2] = 4.0/9; bvals[3] = 0;
		vector bvalsstar  = new vector(4); bvalsstar[0] = 7.0/24; bvalsstar[1] = 1.0/4; bvalsstar[2] = 1.0/3; bvalsstar[3] = 1.0/8;	
		matrix avals = new matrix(4,4); avals.set_zero();	
		avals[1,0] = 0.5; avals[3,0] = 2.0/9; avals[2,1] = 3.0/4; avals[3,1] = 1.0/3; avals[3,2] = 4.0/9; //Bogacki-Shampine method
		vector c = new vector(4);
		c[0] = 0; c[1] = 0.5; c[2] = 0.75; c[3] = 1;
		
		vector k0 = h*this.f(xn,yn);
		vector k1 = h*this.f(xn+c[1]*h, yn+avals[1,0]*k0);
		vector k2 = h*this.f(xn+c[2]*h, yn+avals[2,0]*k0+avals[2,1]*k1);
		vector k3 = h*this.f(xn+c[3]*h, yn+avals[3,0]*k0+avals[3,1]*k1+avals[3,2]*k2);
		ynplus_up += bvals[0]*k0+bvals[1]*k1+bvals[2]*k2+bvals[3]*k3;
		ynplus_lo += bvalsstar[0]*k0+bvalsstar[1]*k1+bvalsstar[2]*k2+bvalsstar[3]*k3;
		err = ynplus_up - ynplus_lo;
		
		vector[] results = {ynplus_up, err};
		return results;
	}

	public Tuple<List<double>, List<vector>, List<vector>> Driver(vector y_start)
	{	//Monitors local errors and tolerances 
		//Adjusts step sizes 
		double hD = (this.b-this.a)/50;
		bool running = true;
		List<double> xdriver = new List<double>(){this.a}; List<vector> ydriver = new List<vector>(){y_start}; List<vector> Edriver = new List<vector>(){y_start*0};
		do
		{
			if(xdriver.Last()+hD > this.b){hD = this.b - xdriver.Last(); running = false;}
			if(hD == 0){hD = this.b - xdriver.Last(); running = false;}
			
			vector[] PossibleStep = Stepper(ydriver.Last(),hD,xdriver.Last());
			double LocalTolerance = (this.eps*PossibleStep[0].norm()+this.acc)*Sqrt(hD/(this.b-this.a));
			double LocalError 	 = PossibleStep[1].norm();
			if(LocalError <= LocalTolerance)
			{
				xdriver.Add(xdriver.Last()+hD); ydriver.Add(PossibleStep[0]); Edriver.Add(PossibleStep[1]);
			}
			if(LocalError > 0)
			{
				hD = hD*Pow(LocalTolerance/LocalError,0.25)*0.95;
			}
		}while(running);
		return Tuple.Create(xdriver,ydriver,Edriver);
	}
}
