using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
class main{

	static void Main()
	{
		double N = 5.806e6; 	//population
		double Tr = 14; 		//days
		double Tcsocdist = 5;	//time between contacts
		double Tcnosocdist = 1;	//time between contacts
		Func<double,vector,vector> fsocdist = (t,y) => new vector(-y[0]*y[1]/(N*Tcsocdist), y[0]*y[1]/(N*Tcsocdist) - y[1]/Tr, y[1]/Tr);			
		double start = 0.0, end = 300;  	
		vector y_start = new vector(N*0.9999, N*0.0001, 0);
		double acc = 1e-5, eps = 1e-5;

		rkstepXY SIRsocdist = new rkstepXY(fsocdist,start,end,y_start, acc,eps);
		List<double> xssocdist = SIRsocdist.xValues; 
		List<vector> yssocdist = SIRsocdist.yValues; 
		//List<vector> errssocdist = SIRsocdist.Errors;

		Func<double,vector,vector> fnosocdist = (t,y) => new vector(-y[0]*y[1]/(N*Tcnosocdist), y[0]*y[1]/(N*Tcnosocdist) - y[1]/Tr, y[1]/Tr);			
		rkstepXY SIRnosocdist = new rkstepXY(fnosocdist,start,end,y_start, acc,eps);
		List<double> xsnosocdist = SIRnosocdist.xValues; 
		List<vector> ysnosocdist = SIRnosocdist.yValues; 
		//List<vector> errsnosocdist = SIRnosocdist.Errors;

		for(int i = 0; i<xssocdist.Count; i++){Error.WriteLine("{0}\t{1}\t{2}\t{3}",xssocdist[i],yssocdist[i][0],yssocdist[i][1],yssocdist[i][2]);}
		Error.WriteLine("\n");
		for(int i = 0; i<xsnosocdist.Count; i++){Error.WriteLine("{0}\t{1}\t{2}\t{3}",xsnosocdist[i],ysnosocdist[i][0],ysnosocdist[i][1],ysnosocdist[i][2]);}
		
		//WriteLine("\n\nTo see the result of the calculation done in B open SIRmodel.svg. Two SIRmodels are shown one with a high time between contacts (Tc) and one with a low time between contacts.");
	}

	static bool CloseEnough(double trialvalue, double goal, double acc)
	{	
		if(Abs(trialvalue - goal) <= acc)
			return true;
		else 
		{
			Error.WriteLine("Trialvalue = {0}, goal = {1}, acc = {2}",trialvalue,goal,acc);
			return false;
		}		
	}
}

