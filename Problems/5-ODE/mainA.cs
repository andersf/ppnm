using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
class main{

	static void Main()
	{
		WriteLine("Part A:");
		Func<double,vector,vector> f = (t,y) => new vector(y[1], -y[0]);			
		double start = 0.0, end = 2*PI;  	
		vector y_start = new vector(0.0, 1.0);
		double acc = 1e-5, eps = 1e-5;

		rkstepXY SinCos = new rkstepXY(f,start,end,y_start, acc,eps);
		List<double> xs = SinCos.xValues; 
		List<vector> ys = SinCos.yValues; 
		List<vector> errs = SinCos.Errors; 
		for(int i = 0; i<xs.Count; i++){Error.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",xs[i],ys[i][0],ys[i][1],errs[i][0],errs[i][1]);}
		WriteLine("Differential equation u''=-u:");
		WriteLine($"acc = {acc}, eps = {eps}");
		WriteLine($"a      = {start}, b      = {end}");
		WriteLine($"y0(a)  = {y_start[0]}, y1(a)  = {y_start[1]}");
		WriteLine($"y0(b)  = {ys[ys.Count-1][0]}, y1(b)  = {ys[ys.Count-1][1]}");
		WriteLine($"sin(a) = {Sin(start)}, cos(a) = {Cos(start)}");
		WriteLine($"sin(b) = {Sin(end)}, cos(b) = {Cos(end)}");
		WriteLine($"sin(b) = {Sin(end)}, cos(b) = {Cos(end)}");

		WriteLine("\n\nTo see the result of the calculation done in A open SinCos.svg and SinCosErr.svg");

	}
}
