using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		//1/x^2 from -inf to -1
		Comparor( (x) => 1/(x*x),
			functag:"1/(x^2)",
			a:-Double.PositiveInfinity, 
			b:-1, 
			abs:1e-6, 
			eps:1e-6,
			trueval:1.0);

		//Exp(-x) from 0 to inf.
		Comparor( (t) => Exp(-t),
			functag:"Exp(-x)",
			a:0, 
			b:Double.PositiveInfinity, 
			abs:1e-6, 
			eps:1e-6,
			trueval:1.0);
		
		//Gauss from -inf to inf
		Comparor( (t) => Exp(-t*t),
			functag:"Exp(-x*x)",
			a:-Double.PositiveInfinity, 
			b:Double.PositiveInfinity, 
			abs:1e-6, 
			eps:1e-6,
			trueval:Sqrt(PI));
		
	}	

	static bool CloseEnough(double trialvalue, double goal, double acc)
	{	
		if(Abs(trialvalue - goal) <= acc)
			return true;
		else 
		{
			Error.WriteLine("Trialvalue = {0}, goal = {1}, acc = {2}",trialvalue,goal,acc);
			return false;
		}		
	}

	static void Comparor(Func<double,double> f, string functag, double a, double b, double abs, double eps, double trueval)
	{
		//WriteLine($"Starting comparison between Clenshaw-Curtis and the recursive adaptive integrator in the calculation {functag}");
		Integrators CC = new Integrators(f,a,b,"CC",abs,eps);
		Integrators RA = new Integrators(f,a,b,"Adaptive",abs,eps);
		double tol_CC = abs+eps*Abs(CC.Integral);
		double tol_RA = abs+eps*Abs(RA.Integral);
		int quad_calls = 0; Func<double, double> f_quad = (x) => {quad_calls++; return f(x);};
		Error.Write($"Integration of {functag} from {a} to {b}:\n");
		Error.Write($"\tabs = {abs}, eps = {eps}\n");
		Error.Write($"\t{"result_CC",-11} = {CC.Integral}, {"tolerance",-10}: {tol_CC:e3}\n");
		Error.Write($"\t{"result_RA",-11} = {RA.Integral}, {"tolerance",-10}: {tol_RA:e3}\n");
		//if(functag=="4*Sqrt(1-x*x)") Error.Write($"\t{"result_o8av",-11} = {quad.o8av(f,a,b,acc:abs,eps:eps)}, {"tolerance",-10}: {tol_RA:e3}\n");
		Error.Write($"\t{"result_o8av",-11} = {quad.o8av(f_quad,a,b,acc:abs,eps:eps)}, {"tolerance",-10}: {tol_RA:e3}\n");
		
		Error.Write($"\t{"true value",-11} = {trueval:f6}\n");
	       	Error.Write($"\t{"error_CC",-15}: {CC.Error}\n");
		Error.Write($"\t{"error_Simple",-15}: {RA.Error}\n");
		Error.Write($"\t{"calls_CC",-15}: {CC.N_calls}\n");
		Error.Write($"\t{"calls_Simple",-15}: {RA.N_calls}\n");
		Error.Write($"\t{"calls_o8av",-15}: {quad_calls}\n\n");
	}	
}
