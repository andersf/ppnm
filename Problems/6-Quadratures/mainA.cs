using System;
using static System.Console;
using static System.Math;
class main{

	static void Main()
	{
		//Pi by Adaptive Quadrature with tridivision
		Writer( (u) => 4*Sqrt(1-u*u),
			functag:"4*Sqrt(1-x*x)",
			a:0, 
			b:1, 
			IntegMethod:"Adaptive",
			abs:1e-6, 
			eps:1e-6,
			trueval:3.14159265358979324);

		//Sqrt(t) by Adaptive Quadrature
		Writer( (t) => Sqrt(t),
			functag:"Sqrt(x)",
			a:0, 
			b:1, 
			IntegMethod:"Adaptive",
			abs:1e-6, 
			eps:1e-6,
			trueval:2.0/3);
		
	}	

	static bool CloseEnough(double trialvalue, double goal, double acc)
	{	
		if(Abs(trialvalue - goal) <= acc)
			return true;
		else 
		{
			Error.WriteLine("Trialvalue = {0}, goal = {1}, acc = {2}",trialvalue,goal,acc);
			return false;
		}		
	}

	static void Writer(Func<double,double> f, string functag, double a, double b, string IntegMethod, double abs, double eps, double trueval)
	{
		Integrators instance = new Integrators(f,a,b,IntegMethod,abs,eps);
		double abserr = Abs(instance.Integral-trueval);
		double tol = abs+eps*Abs(instance.Integral);
		Error.Write($"Integration of {functag} from {a} to {b}:\n");
		Error.Write($"\tabs = {abs}, eps = {eps}\n");
		Error.Write($"\t{"result",-10} = {instance.Integral:f6}, {"tolerance",-10}: {tol:e3}\n");
		Error.Write($"\t{"true value",-10} = {trueval:f6}, {"error",-10}: {instance.Error:e3}\n");
		Error.Write($"\tcalls: {instance.N_calls}\n");
		if (tol > abserr)
			Error.Write("Test passed\n\n");
		else 
			Error.Write("Test failed\n\n");
	}	
}
