using System;
using static System.Math;
public class Integrators 
{
	private int n_calls;
	public int N_calls{get{return n_calls;}}
	private double integral, error;
	public double Integral{get{return integral;}}
	public double Error{get{return error;}}

	public Integrators(Func<double,double> f, double a, double b, string method, double abs=1e-6, double eps=1e-6)
	{
		n_calls = 0; error = 0;
		if(method == "CC")
		{
			if(Double.IsInfinity(a) && Double.IsInfinity(b))
				integral = CC( (x) => f(x/(1-x*x))*(1+x*x)/Pow(1-x*x,2), -1, 1, abs, eps); 
			else if(!Double.IsInfinity(a) && Double.IsInfinity(b))
				integral = CC( (x) => f(a+x/(1-x))*1/Pow(1-x,2), 0, 1, abs, eps); 
			else if(Double.IsInfinity(a) && !Double.IsInfinity(b))
				integral = CC( (x) => f(b+x/(1+x))*1/Pow(1+x,2), -1, 0, abs, eps); 
			else 
				integral = CC(f,a,b,abs,eps);
		}
		if(method == "Adaptive")
		{
			if(Double.IsInfinity(a) && Double.IsInfinity(b))
				integral = Adaptive( (x) => f(x/(1-x*x))*(1+x*x)/Pow(1-x*x,2), -1, 1, abs, eps); 
			else if(!Double.IsInfinity(a) && Double.IsInfinity(b))
				integral = Adaptive( (x) => f(a+x/(1-x))*1/Pow(1-x,2), 0, 1, abs, eps); 
			else if(Double.IsInfinity(a) && !Double.IsInfinity(b))
				integral = Adaptive( (x) => f(b+x/(1+x))*1/Pow(1+x,2), -1, 0, abs, eps); 
			else 
				integral = Adaptive(f,a,b,abs,eps);
		}
	}

	public double Adaptive(Func<double,double> f, double a, double b, double abs, double eps)
	{ 
		double 	f2 = f(a+2*(b-a)/6),
			f3 = f(a+4*(b-a)/6);

		return Recursive24(f,a,b,abs,eps,f2,f3);
		
	} 	

	public double CC(Func<double,double> f, double a, double b, double abs, double eps)
	{	
		//Transformation from a,b to -1,1 integral
		Func<double,double> ftrans = (x) => f((b-a)/2*x+(b+a)/2)*(b-a)/2; //f(g(t))*g'(t)
		
		//Transformation from -1,1 to 0,PI
		Func<double,double> fCC = (x) => ftrans(Cos(x))*Sin(x);
		double 	f2 = fCC(2*PI/6),	
			f3 = fCC(4*PI/6);
		return Recursive24(fCC,0,PI,abs,eps,f2,f3);
	}

	public double Recursive24(Func<double,double> func, double start, double end, double abs, double eps, double f2, double f3)
	{
		this.n_calls++;
		double f1 = func(start+(end-start)/6), f4 = func(start+5*(end-start)/6);
		double 	Q = (2*f1+f2+f3+2*f4)/6*(end-start),
			q = (f1+f2+f3+f4)/4*(end-start);
		
		if(Abs(Q-q) < (abs + eps*Abs(Q)))
		{
			error = Sqrt(error*error+(Q-q)*(Q-q));
			return Q;
		}
		else
		{ //n_calls++;
			return Recursive24(func,start,(start+end)/2,abs/Sqrt(2.0),eps,f1,f2) + Recursive24(func,(start+end)/2,end,abs/Sqrt(2.0),eps,f3,f4);
		}//End else
	}//End Adapt
} //end class
	


