using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.Linq;
class main{

	static void Main()
	{
		//Pi by Adaptive Quadrature with tridivision
		Writer( (u) => 4*Sqrt(1-u*u),
			functag:"4*Sqrt(1-x*x)",
			a:0, 
			b:1, 
			IntegMethod:"Adaptive_tridivision",
			abs:1e-6, 
			eps:1e-6,
			trueval:3.14159265358979324);

		//Sqrt(q) by Adaptive Quadrature with tridivision
		Writer( (t) => Sqrt(t),
			functag:"Sqrt(x)",
			a:0, 
			b:1, 
			IntegMethod:"Adaptive_tridivision",
			abs:1e-6, 
			eps:1e-6,
			trueval:2.0/3);
	}	
	
	
	static void Writer(Func<double,double> f, string functag, double a, double b, string IntegMethod, double abs, double eps, double trueval)
	{
		Integrators instance = new Integrators(f,a,b,IntegMethod,abs,eps);
		double abserr = Abs(instance.Integral-trueval);
		double tol = abs+eps*instance.Integral;
		Write($"Integration of {functag} from {a} to {b} using recursive adaptive integration with tridivision:\n");
		Write($"\tabs = {abs}, eps = {eps}\n");
		Write($"\n\t{"result",-10} = {instance.Integral:f6}, {"tolerance",-10}: {tol:e3}\n");
		Write($"\t{"true value",-10} = {trueval:f6}, {"error",-10}: {abserr:e3}\n");
		Write($"\tcalls: {instance.N_calls}\n");
		if (tol > abserr)
			Write("Test passed\n\n");
		else 
			Write("Test failed\n\n");
	}	
}

