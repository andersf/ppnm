using System;
using static System.Math;
using System.IO;
using System.Linq;
using System.Collections.Generic;
public class Integrators 
{
	private int n_calls;
	public int N_calls{get{return n_calls;}}
	private double integral;
	public double Integral{get{return integral;}}

	public Integrators(Func<double,double> f, double a, double b, string method, double abs, double eps)
	{
		n_calls = 0;
		Func<double,double> F = (x) => {this.n_calls++;return f(x);};
		if(method == "Adaptive_tridivision")
		{	
			if(Double.IsInfinity(a) && !Double.IsInfinity(b))
				integral = Adaptive_tridivision( (x) => F(b+x/(1+x))*1/Pow(1+x,2), -1, 0, abs, eps); 
			else 
				integral = Adaptive_tridivision(F,a,b,abs,eps);
		}
		else
			Console.Error.WriteLine("Input method not recognized. Try 'Adaptive_tridivision'");
	}//Integrators. 

	public double Adaptive_tridivision(Func<double,double> f, double a, double b, double abs, double eps)
	{ 
		double 	f2 = f(a+ 3*(b-a)/6);
		return Adaptive24_tridivision(f,a,b,abs,eps,f2);
	}//Adaptive_tridivision 	
	 //To be able to reuse the calculations like in the bidivision the three intervals each get the middle of the interval calculated.  

	public double Adaptive24_tridivision(Func<double,double> f, double a, double b, double abs, double eps, double f2)
	{
		double  f1 = f(a+ 1*(b-a)/6),
			f3 = f(a+5*(b-a)/6);
		double Q = (2*f1+f2+2*f3)/5*(b-a), q = (f1+f2+f3)/3*(b-a);
		double tolerance = abs+eps*Abs(Q), err = Abs(Q-q);
		
		if(err < tolerance) 
		{
			return Q;
		}//if
		else
		{ 
			 return Adaptive24_tridivision(f,a          ,a+(b-a)/3  ,abs/Sqrt(3.0) ,eps ,f1) + 
				Adaptive24_tridivision(f,a+(b-a)/3  ,a+(b-a)/3*2,abs/Sqrt(3.0) ,eps ,f2) + 
				Adaptive24_tridivision(f,a+(b-a)/3*2,b          ,abs/Sqrt(3.0) ,eps ,f3); 
		}//else 
		//Note that the change of the absolute accuracy goal changes by 1/Sqrt(3) instead of 1/Sqrt(2) due to the change in the splitting of the intervals.
	}//Adaptive24_tridivision
	
}//Class
	


